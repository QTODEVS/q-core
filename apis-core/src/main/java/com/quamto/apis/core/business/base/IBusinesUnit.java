package com.quamto.apis.core.business.base;

import com.quamto.core.Result;
import com.quamto.entity.BaseEntity;
import com.quamto.entity.BaseEntityDAO;

public interface IBusinesUnit {

	public BaseEntityDAO getDAOInstance() throws Exception;
	public Result get(Long id);
	public Result getAll();
	public Result add(BaseEntity entity);
	public Result update(BaseEntity entity);
	public Result delete(Long id);
	
}
