package com.quamto.apis.core.security;


import com.quamto.core.Credential;
import com.quamto.core.QException;
import com.quamto.core.QException.ExceptionType;
import com.quamto.core.Result;

import javax.servlet.http.HttpServletRequest;

/**
 * Class assigned to control the users privileges
 * @author jholguin
 * @since 17/12/2015
 *
 */
public class SecurityManager {
	
	private static String className = "";
	
	public SecurityManager() {
		className = SecurityManager.class.getName();
	}
	
	/**
	 * Validate if the authenticated user has the privileges to complete the operation
	 * @param request Http request that contains the URL requested
	 */
	public static Result validateRequestPrivileges(HttpServletRequest request){
		
		Result result = new Result();
		
		try {
			Credential Credential = getRequestCredential(request);
			
			result.setCredentials(Credential);
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.OperationFailed_err,
					className + "-validateRequestPriviles"));
		}
		return result;
	}
	
	/**
	 * Get the credential for the actual request
	 * @param request Http request information
	 * @return Credential from the request
	 */
	public static Credential getRequestCredential(HttpServletRequest request) throws Exception{
		Credential credential = null;
		try {
			credential = new Credential();
		} catch (Exception e) {
			throw e;
		}
		return credential;
	}
}
