package com.quamto.apis.core.base;

/**
 * Class that contains the base methods for the REST controllers classes
 * 
 * @author jholguin
 * @since 11/02/2016
 *
 */
public abstract class ControllerRESTBase extends ControllerBase {

	public ControllerRESTBase(Class<?> subClass) {
		super(subClass);
	}
	
	public ControllerRESTBase(String subClassName) {
		super(subClassName);
	}
	
}
