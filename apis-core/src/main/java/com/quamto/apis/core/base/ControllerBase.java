package com.quamto.apis.core.base;

import com.quamto.core.Result;
import javax.servlet.http.HttpServletRequest;

import com.quamto.apis.core.security.SecurityManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class that contains the base methods for the controllers classes
 * 
 * @author jholguin
 * @since 19/06/2016
 *
 */
public class ControllerBase {

	protected String className = "";
	protected Logger logger = LogManager.getLogger(ControllerBase.class);

	public ControllerBase(Class<?> subClass) {
		this(subClass.getName());
	}

	public ControllerBase(String subClassName) {
		className = subClassName;
	}

	/**
	 * Validates Session status
	 * @param request Http request
	 * @return ResultadoOPR with the validation
	 */
	public Result validateRequestPrivileges(HttpServletRequest request){
		Result result = new Result();
		try {
			result = SecurityManager.validateRequestPrivileges(request);
		} catch (Exception e) {
			result = new Result(e, className + "-validateRequestPrivileges");
		}
		return result;
	}

	/**
	 * Translate a result operation
	 * @param result Result that contains the operation results
	 * @return result translated
	 */
	public Result translateResult(Result result){
		if(result != null){
			return result.translateLocaleMessages();
		}else{
			return null;
		}
	}
	
}
