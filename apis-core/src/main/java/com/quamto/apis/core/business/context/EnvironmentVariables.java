package com.quamto.apis.core.business.context;

import com.quamto.db.DbConfig;
import org.apache.tomcat.jdbc.pool.DataSource;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * This class is responsible for storing the environment variables for business module
 * @author jholguin
 * @since 16/12/2015
 *
 */
public abstract class EnvironmentVariables {

	private static String applicationPhysicalPath;
	private static String applicationVirtualPath;
	private static String applicationDataPhysicalPath;
	private static String applicationConfigFilePath;

	private static DataSource dbDataSource;
	private static DbConfig dbConfiguration;
	private static Dictionary<String, Object> customizeVariables = null;

	/**
	 * @return the current data source configured to access to the database
	 */
	public static DataSource getDbDataSource(){
		return dbDataSource;
	}
	
	/**
	 * Sets the datasource in the environment variables
	 */
	public static void setDbDataSource(DataSource value){
		EnvironmentVariables.dbDataSource = value;
	}
	
	/**
	 * @return the current physical path of the application
	 */
	public static String getApplicationPhysicalPath() { return applicationPhysicalPath; }
	/**
	 * @param value current physical path of the application
	 */
	public static void setApplicationPhysicalPath(String value) {
		EnvironmentVariables.applicationPhysicalPath = value;
	}
	/**
	 * @return the virtual path of the application
	 */
	public static String getApplicationVirtualPath() {
		return applicationVirtualPath;
	}
	/**
	 * @param value virtual path of the application
	 */
	public static void setApplicationVirtualPath(String value) {
		EnvironmentVariables.applicationVirtualPath = value;
	}
	/**
	 * @return the physical path where the data is stored in the application
	 */
	public static String getApplicationDataPhysicalPath() {
		return applicationDataPhysicalPath;
	}
	/**
	 * @param value physical path where the data is stored in the application
	 */
	public static void setApplicationDataPhysicalPath(String value) {
		EnvironmentVariables.applicationDataPhysicalPath = value;
	}
	/**
	 * @return Wing configuration database connection
	 */
	public static DbConfig getDbConfiguration() {
		return dbConfiguration;
	}
	/**
	 * @param value Wing configuration database connection
	 */
	public static void setDbConfiguration(DbConfig value) {
		EnvironmentVariables.dbConfiguration = value;
	}
	/**
	 * @return File Path application settings
	 */
	public static String getApplicationConfigFilePath() {
		return applicationConfigFilePath;
	}
	/**
	 * @param value File Path application settings
	 */
	public static void setApplicationConfigFilePath(String value) {
		EnvironmentVariables.applicationConfigFilePath = value;
	}

	/**
	 * Add customize variables that can contains object elements
	 * @param key Key unique variable
	 * @param value Object value
	 */
	public static void addCustomizeVariable(String key, Object value){
		if(customizeVariables == null)
			customizeVariables = new Hashtable<>();
		customizeVariables.put(key, value);
	}

	/**
	 * Gets element storage in the customize variables
	 * @param key unique key
	 * @return
	 */
	public static Object getCustomizeVariable(String key){
		if(customizeVariables != null)
			return customizeVariables.get(key);
		return null;
	}
}

