package com.quamto.apis.core.business.context;


import com.quamto.core.QException;
import com.quamto.db.DbConfig;
import com.quamto.db.DbConnection;
import org.apache.tomcat.jdbc.pool.DataSource;


/**
 * Class that manages the pool database connection 
 * 
 * @author jholguin
 * @since 17/12/2015
 *
 */
public class DbConnectionManager {
	
	/**
	 * Gets an open connection available 
	 * 
	 * @return Connection to open database
	 * @throws QException Throws an exception if there are problems when opening
	 */
	public static DbConnection getAvailableDbConnection() throws QException {
		
		DbConnection dbConnection = null;
		
		try {
			 
			DataSource ds = EnvironmentVariables.getDbDataSource();
			DbConfig dbConfig = EnvironmentVariables.getDbConfiguration();

			if(dbConfig == null && ds != null){
				dbConfig = DbConnection.getDbConfigFromDataSource(ds);
				EnvironmentVariables.setDbConfiguration(dbConfig);
			}

			dbConnection = new DbConnection(ds, dbConfig);
			dbConnection.connect();
		} catch (QException e) {
			throw e;
		} 
		return dbConnection;
	}
	
	/**
	 * Releases the connection to the database used
	 * 
	 * @param dbConnection Connection to be closed
	 */
	public static void releaseDbConnection(DbConnection dbConnection){
		
		try {
			dbConnection.closeConnection();
			dbConnection = null;
		} catch (QException e) {
			QException.printLogException(e);
		}
	}
}
