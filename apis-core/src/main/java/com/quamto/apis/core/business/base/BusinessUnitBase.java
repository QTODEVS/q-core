package com.quamto.apis.core.business.base;

import com.quamto.apis.core.business.context.DbConnectionManager;
import com.quamto.core.QException.ExceptionType;
import com.quamto.core.Credential;
import com.quamto.core.QException;
import com.quamto.core.Result;
import com.quamto.db.DbConnection;
import com.quamto.db.DbOperator;
import com.quamto.entity.BaseEntity;
import com.quamto.entity.BaseEntityDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;

import java.lang.reflect.Constructor;
import java.sql.Savepoint;

/**
 * @author jholguin on 12/06/17.
 * @since 12/06/2017
 */
public class BusinessUnitBase implements AutoCloseable, IBusinesUnit {


    protected Logger logger = LogManager.getLogger(BusinessUnitBase.class);
    protected BaseEntityDAO entityDAO = null;
    protected String className = "";
    protected DbConnection dbConnection = null;
    protected Credential credentials = null;
    protected Class<?> classDAO = null;
    protected Class<?> classModel = null;
    protected Savepoint savepoint = null;
    protected DbOperator dbOperator = null;


    public BusinessUnitBase(Credential credentials, String subClassName){
        this.className = subClassName;
        this.credentials = credentials;
        try {
            setupDbConnection();
            setupDbOperator();
        }catch(Exception e){
            QException.printLogException(e);
        }
    }

    public BusinessUnitBase(Class<?> classDAO, Credential credentials, String subClassName) {
        this.className = subClassName;
        this.classDAO = classDAO;
        this.credentials = credentials;
        try {
            setupDbConnection();
            setupDbOperator();
        }catch(Exception e){
            QException.printLogException(e);
        }
    }

    public BusinessUnitBase(Class<?> classDAO, DbConnection dbConnection, Credential credentials, String subClassName) {
        this.className = subClassName;
        this.classDAO = classDAO;
        this.credentials = credentials;
        this.dbConnection = dbConnection;
        try {
            setupDbOperator();
        }catch(Exception e){
            QException.printLogException(e);
        }
    }

    public BusinessUnitBase(Class<?> classDAO, Class<?> classModel, Credential credentials, String subClassName){
        this.className = subClassName;
        this.classDAO = classDAO;
        this.classModel = classModel;
        this.credentials = credentials;
        try {
            setupDbConnection();
            setupDbOperator();
        }catch(Exception e){
            QException.printLogException(e);
        }
    }

    /**
     * Gets a DAO instance
     * @return DAO instance initiated
     * @throws Exception
     */
    public BaseEntityDAO getDAOInstance() throws Exception{
        BaseEntityDAO dao = null;
        try {
            Constructor<?> ctor = classDAO.getConstructor(DbConnection.class);
            dao = (BaseEntityDAO) ctor.newInstance(dbConnection);
        } catch (Exception e) {
            throw e;
        }
        return dao;
    }

    /**
     * Get entity object by the identifier
     * @param id Entity identifier
     * @return Entity instance
     */
    public Result get(Long id){
        Result result = new Result();
        try {
            try(BaseEntityDAO daoInstance = getDAOInstance()){
                result.setCredentials(credentials);
                if(this.classModel != null){
                    result.setPayload(new DozerBeanMapper().map(daoInstance.get(id), this.classModel));
                }else{
                    result.setPayload(daoInstance.get(id));
                }

            }
        } catch (Exception e) {
            result = new Result(new QException(e, ExceptionType.OperationFailed_err,
                    className + "-get"));
        }
        return result;
    }

    /**
     * Gets all entities list stored
     * @return Result perform an operation
     */
    public Result getAll(){
        Result result = new Result();
        try {
            try(BaseEntityDAO daoInstance = getDAOInstance()){
                result.setCredentials(credentials);
                result.setPayload(daoInstance.getAll());
            }
        } catch (Exception e) {
            result = new Result(new QException(e, ExceptionType.OperationFailed_err,
                    className + "-getAll"));
        }
        return result;
    }

    /**
     * Method providing the service to add an instance of the class
     *
     * @param entity Entity to add
     * @return Result perform an operation add an element
     */
    public Result add(BaseEntity entity) {
        Result result = new Result();
        try {
            try(BaseEntityDAO daoInstance = getDAOInstance()){
                result.setCredentials(credentials);
                result = daoInstance.add(entity);
            }
        } catch (Exception e) {
            result = new Result(new QException(e, ExceptionType.OperationFailed_err,
                    className + "-add"));
        }
        return result;
    }

    /**
     * Method providing the service to update an instance of the class
     *
     * @param entity Entity to update
     * @return Result perform an operation add an element
     */
    public Result update(BaseEntity entity) {
        Result result = new Result();
        try {
            try(BaseEntityDAO daoInstance = getDAOInstance()){
                result.setCredentials(credentials);
                result = daoInstance.update(entity);
            }
        } catch (Exception e) {
            result = new Result(new QException(e, ExceptionType.OperationFailed_err,
                    className + "-update"));
        }
        return result;
    }

    /**
     * Method providing the service to delete an instance of the class
     *
     * @param id entity identifier
     * @return Result perform an operation add an element
     */
    public Result delete(Long id) {
        Result result = new Result();
        try {
            try(BaseEntityDAO daoInstance = getDAOInstance()){
                result.setCredentials(credentials);
                result = daoInstance.delete(id);
            }
        } catch (Exception e) {
            result = new Result(new QException(e, ExceptionType.OperationFailed_err,
                    className + "-delete"));
        }
        return result;
    }

    /**
     * Gets a map array from a query
     * @param query
     * @return
     */
    protected Result getMapArrayFromQuery(String query){
        Result result = new Result();
        try {
            result.setPayload(dbOperator.getMapArrayFromQuery(query));
        }catch (QException e){
            result.setException(e);
        }
        return result;
    }

    /**
     * Setups the dbConnection to use in the controller
     */
    protected void setupDbConnection() throws Exception {
        try {
            if(dbConnection == null){
                dbConnection = DbConnectionManager.getAvailableDbConnection();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Sets the database operator to run database operations by queries
     * @throws Exception
     */
    private void setupDbOperator() throws Exception{
        try{
            if(dbConnection != null){
                dbOperator = new DbOperator(dbConnection);
            }else{
                throw new Exception("Database connection is null");
            }
        }catch(Exception e){
            throw e;
        }
    }

    /**
     * Return a database Connection
     * @return a database Connection
     */
    protected DbConnection getDbConnection() throws Exception {
        setupDbConnection();
        return dbConnection;
    }

    /**
     * Release the database connection after of this use
     */
    protected void releaseDbConnection() {
        if(this.dbConnection!=null){
            try {
                this.dbConnection.closeConnection();
                this.dbConnection = null;
            } catch (Exception e) {
                QException.printLogException(e);
            }
        }
    }

    /**
     * Release the database connection after of this use
     * @param dbConnection Database connection
     */
    protected void releaseDbConnection(DbConnection dbConnection) {
        if(dbConnection!=null){
            try {
                dbConnection.closeConnection();
                dbConnection = null;
            } catch (Exception e) {
                QException.printLogException(e);
            }
        }
    }

    /**
     * Start a database transaction
     * @return Operation result
     */
    protected Result beginDbTransaction(){
        Result result = new Result();
        try {
            dbConnection.setAutoCommit(false);
            savepoint = dbConnection.setSavepoint();
        }catch (Exception e){
            result = new Result(e);
        }
        return result;
    }

    /**
     * Commit the actual transaction
     * @return Operation result
     */
    protected Result commitDbTransaction(){
        Result result = new Result();
        try {
            dbConnection.commit();
            savepoint = null;
            dbConnection.setAutoCommit(true);
        }catch (Exception e){
            result = new Result(e);
        }
        return result;
    }

    /**
     * Rollback the actual transaction
     * @return Operation result
     */
    protected Result RollBackDbTransaction(){
        Result result = new Result();
        try {
            dbConnection.rollBack(savepoint);
            savepoint = null;
            dbConnection.setAutoCommit(true);
        }catch (Exception e){
            result = new Result(e);
        }
        return result;
    }

    @Override
    public void close() throws Exception {
        try {
            entityDAO = null;
            releaseDbConnection();
        } catch (Exception e) {
            throw e;
        }
    }
}
