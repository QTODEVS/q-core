package com.quamto.db;

/**
 * Class that allows encapsulate data types handled in an SQL query with their respective values
 * 
 * @author jholguin
 * @since 19/05/2015
 *
 */
public class SQLParameter {
	
	/**
	 * It defines the types of data they can use in a SQL statement to insert, update, select, etc.
	 * 
	 * @author jholguin
	 * @since 15/05/2015
	 *
	 */
	public enum SQLDataType{
		String_sdt,
		Integer_sdt,
		Long_sdt,
		Decimal_sdt,
		Date_sdt,
		DateTime_sdt,
		AutoIncrement_sdt,
		Boolean_sdt
	}
	
	private String fieldName;
	private Object value;
	private SQLDataType type;
	private Boolean mandatory;
	private String sqlConditionAI;
	
	/**
	 * 
	 * @param fieldName Field name that is associated with the SQL parameter
	 * @param value Value assigned to the field
	 * @param type Type value assigned to the SQL parameter field
	 */
	public SQLParameter(String fieldName, Object value, SQLDataType type){
		this(fieldName, value, type, false);
	}
	
	/**
	 * 
	 * @param fieldName Field name that is associated with the SQL parameter
	 * @param value Value assigned to the field
	 * @param type Type value assigned to the SQL parameter field
	 * @param isMandatory Establish if the field is mandatory
	 */
	public SQLParameter(String fieldName, Object value, SQLDataType type, Boolean isMandatory){
		this(fieldName, value, type, isMandatory, "");
	}
	
	/**
	 * 
	 * @param fieldName Field name that is associated with the SQL parameter
	 * @param value Value assigned to the field
	 * @param type Type value assigned to the SQL parameter field
	 * @param isMandatory Establish if the field is mandatory
	 */
	public SQLParameter(String fieldName, Object value, SQLDataType type, Boolean isMandatory, String sqlConditionAI){
		this.fieldName = fieldName;
		this.value = value;
		this.type = type;
		this.mandatory = isMandatory;
		this.sqlConditionAI = sqlConditionAI;
	}
	
	/**
	 * @return Field name that is associated with the SQL parameter
	 */
	public String getFieldName() {
		return fieldName;
	}
	/**
	 * @param value Field name that is associated with the SQL parameter
	 */
	public void setFieldName(String value) {
		this.fieldName = value;
	}
	/**
	 * @return Value assigned to the field
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value assigned to the field
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	/**
	 * @return Type value assigned to the SQL parameter field
	 */
	public SQLDataType getType() {
		return type;
	}
	/**
	 * @param value Type value assigned to the SQL parameter field
	 */
	public void setType(SQLDataType value) {
		this.type = value;
	}


	/**
	 * @return If you required the data type and should be defined a value
	 */
	public Boolean getIsMandatory() {
		return mandatory;
	}

	/**
	 * @param value If you required the data type and should be defined a value
	 */
	public void setIsMandatory(Boolean value) {
		this.mandatory = value;
	}

	/**
	 * @return SQL condition to be applied to parameters for Auto-increment build
	 */
	public String getSQLConditionAI() {
		return sqlConditionAI;
	}

	/**
	 * @param value SQL condition to be applied to parameters for Auto-increment build
	 */
	public void setSQLConditionAI(String value) {
		this.sqlConditionAI = value;
	}
	
	
	
	
}
