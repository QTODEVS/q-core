package com.quamto.db;


import java.sql.Connection;
import java.sql.Savepoint;
import java.sql.Statement;


import com.mysql.cj.jdbc.MysqlDataSource;
import com.quamto.core.QException;
import com.quamto.core.QException.ExceptionType;
import com.quamto.db.DbConfig.DbEngines;
import com.quamto.util.TypesValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;

/**
 * Class that contains the database connection
 * @author jholguin
 * @since 16/12/2015
 */
public class DbConnection {
	
	private final String className = "DbConnection";

	private static Logger logger = LogManager.getLogger(DbConnection.class);
	private DbConfig dbConfig;
	private String driver;
	private Connection connection;
	private DataSource dataSource;
	
	public DbConnection(DataSource dataSource, DbConfig dbConfig){
		this.dataSource = dataSource;
		this.dbConfig = dbConfig;
	}
	
	/**
	 * Constructor that initializes the parameters of connection to the database
	 * @param engine Type of engine to which the connection is made
	 * @param dbName Name of the database where the connection is made
	 * @param host Route host where the database engine is located
	 * @param user User authentication in the connection
	 * @param password Password of the database
	 */
	public DbConnection(DbEngines engine, String schema, String dbName, String host, String user, String password, String port){
		this(new DbConfig(engine, schema, dbName, host, user, password, port));
	}
	
	/**
	 * Initializes the object with the database configuration
	 * 
	 * @param dbConfig Object that contains the database configuration
	 */
	public DbConnection(DbConfig dbConfig){
		this.dbConfig = dbConfig;
		
		String dbDriver = "";
		String dbPort = "";
		
		switch (this.dbConfig.getEngine()) {
			case MySQL:
				dbDriver = "com.mysql.jdbc.Driver";
				dbPort = "3306";
				break;
	
			default:
				break;
		}
		this.driver = dbDriver;
		
		if(TypesValidator.isNullOrEmpty(this.dbConfig.getPort()))
		{
			this.dbConfig.setPort(dbPort);
		}
	}
	
	
	
	/**
	 * Make the connection to the database according to the parameters defined in the class
	 * 
	 * @throws QException an exception type core Kong
	 */
	public void connect() throws QException{
		
		try{
			if(dataSource == null){
				String url = "";
				
				switch (this.dbConfig.getEngine()) {
				case MySQL:
					url = "jdbc:mysql://"+this.dbConfig.getHost().toString()+":"+this.dbConfig.getPort().toString()+"/"+this.dbConfig.getSchema().toString();
				    MysqlDataSource dsMySQL = new MysqlDataSource();
					
				    dsMySQL.setUser(this.dbConfig.getUser());
				    dsMySQL.setPassword(this.dbConfig.getPassword());
				    dsMySQL.setUrl(url);
				    dataSource = (DataSource)dsMySQL;
					break;
				default:
					break;
				}
			}else{
				if(dbConfig == null){
					dbConfig = getDbConfigFromDataSource(dataSource);
				}
			}

			
			connection = dataSource.getConnection();
		}catch(Exception ex){
			throw new QException(ex, ExceptionType.DatabaseOperation_err, className + "-" + "connect");
		}
	}

	/**
	 * Gets Database configuration from given datasource
	 * @return
	 */
	public static DbConfig getDbConfigFromDataSource(DataSource ds){
		try{
			logger.info("Setting database configuration");
			DbConfig dbConfig = new DbConfig();
			if(ds != null){
				org.apache.tomcat.jdbc.pool.DataSource dsPool = (org.apache.tomcat.jdbc.pool.DataSource)ds;
				String[] parameters = dsPool.getUrl().split(":");
				if(parameters.length >= 4){
					String engineStr = parameters[1];
					String hostStr = parameters[2].replace("/", "");
					String portStr = parameters[3].split("/")[0];
					String schemaStr = parameters[3].split("/")[1];
					if(engineStr.toUpperCase().equals("MYSQL")){
						dbConfig.setEngine(DbEngines.MySQL);
					}
					dbConfig.setHost(hostStr);
					dbConfig.setPort(portStr);
					dbConfig.setSchema(schemaStr);
				}
				dbConfig.setUser(dsPool.getUsername());
				dbConfig.setPassword(dsPool.getPassword());
			}
			return dbConfig;
		}catch (Exception e){
			throw e;
		}
	}

	/**
	 * Validates if the database connection is open
	 * @throws Exception Launch exception when something fails
	 */
	public void validateConnectionStatus() throws Exception{
		try {
			if(connection == null){
				connection = dataSource.getConnection();
			}else if(connection.isClosed()){
				connection = dataSource.getConnection();
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Closes the connection to the current database
	 * @throws QException If there is a problem when attempting to perform the operation throws an exception
	 */
	public void closeConnection() throws QException{
		
		try {
			if(connection != null){
				
				connection.close();
			}
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "closeConnection");
		}
	}
	
	/**
	 * Make a confirmation in the database
	 * 
	 * @throws QException Throws an exception if not achieve confirmation database
	 */
	public void commit() throws QException{
		
		try {
			connection.commit();
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, "" + "-" + "commit");
		}
	}
	
	/**
	 * Returns a transaction initiated from a starting point
	 * @param savePoint Point where the transaction was initiated
	 * @throws QException Throws an exception in case of failing to complete the operation
	 */
	public void rollBack(Savepoint savePoint) throws QException{
		
		try {
			
			connection.rollback(savePoint);
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "rollBack");
		}
		
	}
	
	/**
	 * @return The database configuration
	 */
	public DbConfig getDbConfig() {
		return dbConfig;
	}
	
	/**
	 * @return the driver used to connect to the database
	 */
	public String getDriver() {
		return driver;
	}
	
	/**
	 * @return Direct Connection object from the database
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @return the preparedStatement
	 */
	public Statement getStatement() throws Exception {
		validateConnectionStatus();
		return connection.createStatement();
	}
	
	/**
	 * @return An SQL statement to be prepared
	 */
	public Statement getPreparedStatement(String sentenciaSQL) throws Exception {
		validateConnectionStatus();
		return connection.prepareStatement(sentenciaSQL);
	}

	/**
	 * @param autoCommit If a commit is performed automatically by executing SQL statements
	 */
	public void setAutoCommit(Boolean autoCommit) throws QException{
		
		try {
			
			connection.setAutoCommit(autoCommit);
			
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-setAutoCommit");
		}
	}
	
	/**
	 * It sets the starting point of an SQL transaction
	 * @return Starting point of an SQL transaction
	 * @throws QException Throws an exception if it can not establish the starting point
	 */
	public Savepoint setSavepoint() throws QException{
		
		Savepoint savepoint = null;
		
		try {
			
			savepoint = connection.setSavepoint();
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "getSavepoint");
		}
		return savepoint;
	}
	
}
