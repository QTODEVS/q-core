package com.quamto.db;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that contains the database configuration
 * 
 * @author jholguin
 * @since 12/16/2015
 *
 */
public class DbConfig {

	/**
	 * Enumeration containing the database engines supported by the kind of connectivity to the database
	 */
	public enum DbEngines{
		Oracle(0),
		MySQL(1);
		
		private final int intValue;
		private final String description;
		
		private DbEngines(int valor){
			this.intValue = valor;
			
			switch (valor) {
				case 0:
					this.description = "MySQL";
					break;
				default:
					this.description = "";
					break;
			}
			
		}
		
		public int getInt(){
			return intValue;
		}
		
		public String getDescription(){
			return description;
		}
		
		/**
		 * Returns the enum value from integer value
		 * @param value Integer value
		 * @return Enumeration value
		 */
		public DbEngines getEnumValue(int value){
			
			DbEngines enumValue = null;
			
			switch (value) {
				case 0:
					enumValue = DbEngines.Oracle;
					break;
				case 1:
					enumValue = DbEngines.MySQL;
					break;
			}
			return enumValue;
		}
		
		
		/**
		 * Gets map with the enum values and description
		 * 
		 */
		public Map<DbEngines, String> getMapEnumValues(){
			
			Map<DbEngines, String> mapEnum = new HashMap<DbEngines, String>();
			
			for (DbEngines type : DbEngines.values()) {
				mapEnum.put(type, type.getDescription());
			}
			return mapEnum;
		}
		
		/**
		 * Gets map with the integer values and description
		 * 
		 */
		public Map<Integer, String> getMapIntValues(){
			
			Map<Integer, String> mapEnum = new HashMap<Integer, String>();
			
			for (DbEngines type : DbEngines.values()) {
				mapEnum.put(type.getInt(), type.getDescription());
			}
			return mapEnum;
		}
	}
	
	/**
	 * Attributes of the configuration database
	 */
	private DbEngines engine = DbEngines.MySQL;
	private String host = "";
	private String name = "";
	private String schema = "";
	private String port = "";
	private String user = "";
	private String password = "";
	
	/**
	 * Void class constructor
	 */
	public DbConfig(){
		
	}
	
	/**
	 * Constructor that initializes the parameters of database configuration
	 * @param engine Type of engine to which the connection is made
	 * @param name Name of the database where the connection is made
	 * @param host Route host where the database engine is located
	 * @param user User authentication in the connection
	 * @param password Password of the database
	 * @param port Port of connection
	 */
	public DbConfig(DbEngines engine, String schema, String name, String host, String user, String password, String port){
		this.host = host;
		this.engine = engine;
		this.name = name;
		this.schema = schema;
		this.port = port;
		this.user = user;
		this.password = password;
	}

	/**
	 * @return Type engine database
	 */
	public DbEngines getEngine() {
		return engine;
	}
	
	/**
	 * @return Type engine database
	 */
	public Integer getEngineInt(){
		return engine.getInt();
	}

	/**
	 * @param value Type engine database
	 */
	public void setEngine(DbEngines value) {
		this.engine = value;
	}

	/**
	 * @return Host route which will connect to the database
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param value Host route which will connect to the database
	 */
	public void setHost(String value) {
		this.host = value;
	}

	/**
	 * @return Name database or schema
	 */
	public String getSchema() {
		return schema;
	}

	/**
	 * @return Database name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param value Database name
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * @param value Name database or schema
	 */
	public void setSchema(String value) {
		this.schema = value;
	}

	/**
	 * @return Port connection to the database
	 */
	public String getPort() {
		return port;
	}

	/**
	 * @param value Port connection to the database
	 */
	public void setPort(String value) {
		this.port = value;
	}

	/**
	 * @return User authentication to the database
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param value User authentication to the database
	 */
	public void setUser(String value) {
		this.user = value;
	}

	/**
	 * @return Authentication password to the database
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param value Authentication password to the database
	 */
	public void setPassword(String value) {
		this.password = value;
	}
	
	/**
	 * @return Description of engine type database, obtained from the enumeration
	 */
	public String getEngineDescription(){
		return engine.getDescription();
	}
	
	
}
