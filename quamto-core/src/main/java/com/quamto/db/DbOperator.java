package com.quamto.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quamto.core.QException;
import com.quamto.core.Result;
import com.quamto.core.QException.ExceptionType;
import com.quamto.util.TypesValidator;
import com.quamto.db.DbConfig.DbEngines;
import com.quamto.db.SQLParameter.SQLDataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Class that centralize the database operations
 * 
 * @author jholguin
 * @since 16/12/2015
 */
public class DbOperator {

	/**
	 * Enumeration used to define the database operations that can be performed in the database
	 * 
	 */
	public enum SQLOperations{
		Insert,
		Update,
		Delete,
		Select
	}

	private Logger logger = LogManager.getLogger(DbOperator.class);

	private final DbConnection dbConnection;
	private final String className;
	
	//Variable used to set the starting point of a transaction
	private Savepoint savePoint = null;
	
	//Sets whether a transaction was initiated
	private Boolean transactionInitiated = false;
	
	public DbOperator(DbConnection dbConnection){
		this.dbConnection = dbConnection;
		this.className = this.getClass().getSimpleName();
	}

	/**
	 * @return Conexion de base de datos definida para los operadores SQL
	 */
	public DbConnection getDbConnection() {
		return dbConnection;
	}
	
	/**
	 * @return Gets the database engine of the database connection
	 */
	public DbEngines getDbEngine(){
		return dbConnection.getDbConfig().getEngine();
	}
	
	/**
	 * You get a table obtained result of an SQL statement
	 * @param query SQL statement for the table
	 * @return Table of results obtained from the SQL query
	 * @throws QException Throws an exception if it can not return the table by a problem
	 */
	public ResultSet getResultSet(String query) throws QException{
		
		ResultSet rs = null;
		
		try {
			
			Statement statement = dbConnection.getStatement();
			
			try {
				
				rs = statement.executeQuery(query);
				
			} catch (Exception e) {
				throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "getResultSet", query);
			} 
		} catch (Exception e) {
		    throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "getResultSet", query);
		} 
		return rs;
	}
	
	/**
	 * Executes an SQL statement in the database
	 * 
	 * @param query Sentencia SQL que se desea ejecutar en la base de datos
	 * @return Result of the operation executed in the database
	 */
	public Result executeQuery(String query){
		
		InfoDbOperation infoSQL = new InfoDbOperation();
		Result result = new Result();
		
		try {
			
			if(!query.isEmpty()){
				
				infoSQL.setSentenciaSQL(query);
				
				Statement statement = dbConnection.getStatement();
				 
				try {
					
					infoSQL.setUpdatedRows(statement.executeUpdate(query));
					
				} catch (Exception e) {
					
					result.setException(new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "executeQuery", query));
				} 
			}else{
				result.setException(new QException(ExceptionType.DatabaseOperation_err));
			}
			result.setInfoSQL(infoSQL);
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "executeQuery", query));
		}
		return result;
	}
	
	/**
	 * Build a SQL INSERT statement and run type in the database
	 * 
	 * @param tableName Table into which to insert data
	 * @param params Parameter list of the fields to be inserted into the table
	 * @return Result of the operation and except if existed
	 */
	public Result executeInsertQuery(String tableName, ArrayList<SQLParameter> params){
		
		InfoDbOperation infoSQL = new InfoDbOperation();
		Result result = new Result();
		String insertSQL = "";
		
		PreparedStatement preparedStatement =  null;
		
		try {
			
			String valuesSQL = "";
			String separatorSQL = "";
			
			insertSQL = "INSERT INTO " + tableName + " (";
			
			for (SQLParameter parametroSQL : params) {
				

				//Primary key parameters are omitted because this is a field that is auto-incremental
				if((parametroSQL.getValue() != null) || parametroSQL.getType() == SQLDataType.AutoIncrement_sdt){
					
					insertSQL = insertSQL + separatorSQL + parametroSQL.getFieldName();
					valuesSQL = valuesSQL + separatorSQL + "?";
					separatorSQL = ",";
					
				}
				
			}
			
			insertSQL = insertSQL + ") VALUES (" + valuesSQL + ")";
			
			infoSQL.setSentenciaSQL(insertSQL);
			
			try {
				preparedStatement = getPreparedStatement(insertSQL, params, SQLOperations.Insert, tableName);
			} catch (QException e) {
				throw e;
			}
			
			preparedStatement.execute();
	
			infoSQL.setLastId(getLastInsertedID());
			
			result.setInfoSQL(infoSQL);
			
		} catch (Exception e) {
			
			result.setException(new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "executeInsertQuery", insertSQL));
		} 
		return result;
	}
	
	
	/**
	 * Gets the last record identifier table automatic inserted into the database according to the latest insert executed in the database
	 * 
	 * @return Table identifier automatically created
	 */
	public Long getLastInsertedID(){
		
		Long lastID = 0L;
		
		try {
			
			ResultSet rs = null;
			Statement statement = dbConnection.getStatement();
			
			switch (getDbEngine()) {
				case MySQL:
					rs = statement.executeQuery("SELECT LAST_INSERT_ID() AS Id");
					break;
				default:
					break;

			}
			if(rs != null){
				
				rs.next();
				lastID = rs.getLong("Id");
				
				rs.close();
				rs = null;
			}
			if(statement != null){
				
				statement.close();
				statement = null;
			}
		} catch (Exception e) {
			new QException(e, ExceptionType.OperationFailed_err, className + "-getLastInsertedID");
		}
		return lastID;
	}
	

	/**
	 * Function responsible for building an SQL (Prepared Statement) statement from the SQL parameters and SQL statement sent
	 * 
	 * @param tableName Table name
	 * @param fieldIdName Identifier field name
	 * @param recordID Record identifier
	 * @param param Parameters where the field names, data types and values of each
	 * @return Returns a prepared statement to execute SQL
	 * @throws Exception Launch exception when something fails
	 */
	public Result executeUpdateQuery(String tableName, String fieldIdName, long recordID, ArrayList<SQLParameter> param) throws Exception{
		
		InfoDbOperation infoSQL = new InfoDbOperation();
		Result result = new Result();
		String updateSQL = "";
		
		try {
			
			PreparedStatement preparedStatement = null;
			String separatorSQL = "";
			
			try {
				
				updateSQL = "UPDATE " + tableName + " SET ";
				
				for (SQLParameter parametroSQL : param) {
					
					//Primary key parameters are omitted because this is a field that is auto-incremental
					if(parametroSQL.getValue() != null){
						
						updateSQL = updateSQL + separatorSQL + parametroSQL.getFieldName() + "=?";
						separatorSQL = ",";
					}
				}
				
				updateSQL += " WHERE " + fieldIdName + "=" + recordID;
				
				infoSQL.setSentenciaSQL(updateSQL);
				
				try {
					preparedStatement = getPreparedStatement(updateSQL, param, SQLOperations.Update, tableName);
				} catch (QException e) {
					throw e;
				}
				preparedStatement.execute();
				infoSQL.setUpdatedRows(preparedStatement.getUpdateCount());
			} catch (Exception e) {
				
				throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "executeUpdateQuery", updateSQL);
			}
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "executeUpdateQuery", updateSQL);
		}
		result.setInfoSQL(infoSQL);
		
		return result;
		
	}
	
	/**
	 * Get the next numerical value of a field belonging to a table
	 * 
	 * @param tableName Name of the table to assess
	 * @param fieldName Name field associated with the table that you want to get the value
	 * @return Next numerical value of the table
	 * @throws QException Throws an exception in case you have problems trying to get the next numerical value
	 */
	public Long getNextNumberValue(String tableName, String fieldName) throws QException{
		return getNextNumberValue(tableName, fieldName, "");
	}
	
	/**
	 * Get the next numerical value of a field belonging to a table
	 * 
	 * @param tableName Name of the table to assess
	 * @param fieldName Name field associated with the table that you want to get the value
	 * @param sqlCondition If you want to perform some SQL filter condition set, this starts
     *        without the keyword AND if more elements required condition are concatenated with the AND 
     *        example Condition = "FIELD1 = FIELD2 = value1 AND value2 ..."
	 * 
	 * @return Next numerical value of the table
	 * @throws QException Throws an exception in case you have problems trying to get the next numerical value
	 */
	public Long getNextNumberValue(String tableName, String fieldName, String sqlCondition) throws QException{
		
		Long nextValue = 0L;
		String query = "";
		
		try {
			
			query = "SELECT MAX(" + fieldName + ") AS Ultimo" 
			    + "  FROM " + tableName;
			
			if(!TypesValidator.isNullOrEmpty(sqlCondition)){
				query = query + " WHERE " + sqlCondition;
			}
			
			ResultSet rs = getResultSet(query);
			while(rs.next()){
				
				Long lastValue = 0L;
				if(TypesValidator.isNumeric(rs.getString("Ultimo"))){
					lastValue = Long.parseLong(rs.getString("Ultimo"));
				}
				nextValue = lastValue + 1;
				break;
			}
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-getNextNumberValue", query);
		}
		return nextValue;
	}
	
	/**
	 * You can check if a record exists in a given table
	 * @param tableName Table in which you want to search the registry
	 * @param fieldName Field where the registry value is sought
	 * @param value Value to search within the table and field
	 * @return Whether or not the registration database
	 * @throws QException the exception converted to the internal type of the application
	 */
	public Boolean existValueInTable(String tableName, String fieldName, long value) throws QException{
		return existValueInTable(tableName, fieldName, value, "");
	}
	
	/**
	 * You can check if a record exists in a given table
	 * @param tableName Table in which you want to search the registry
	 * @param fieldName Field where the registry value is sought
	 * @param value Value to search within the table and field
	 * @param sqlCondition Condition to filter the data
	 * @return Whether or not the registration database
	 * @throws QException the exception converted to the internal type of the application
	 */
	public Boolean existValueInTable(String tableName, String fieldName, long value, String sqlCondition) throws QException{
		return existValueInTable(tableName, fieldName, String.valueOf(value), sqlCondition);
	}
	
	/**
	 * You can check if a record exists in a given table
	 * @param tableName Table in which you want to search the registry
	 * @param fieldName Field where the registry value is sought
	 * @param value Value to search within the table and field
	 * @return Whether or not the registration database
	 * @throws QException the exception converted to the internal type of the application
	 */
	public Boolean existValueInTable(String tableName, String fieldName, String value) throws QException{
		return existValueInTable(tableName, fieldName, value, "");
	}
	
	/**
	 * You can check if a record exists in a given table
	 * @param tableName Table in which you want to search the registry
	 * @param fieldName Field where the registry value is sought
	 * @param value Value to search within the table and field
	 * @param sqlCondition Condition to filter the data
	 * @return Whether or not the registration database
	 * @throws QException the exception converted to the internal type of the application
	 */
	public Boolean existValueInTable(String tableName, String fieldName, String value, String sqlCondition) throws QException{
		
		Boolean result = false;
		String query = "";
		
		try {
			
			query = "SELECT " + fieldName + 
					" FROM " + tableName + 
					" WHERE " + fieldName + "='" + value + "'";
			
			if(!TypesValidator.isNullOrEmpty(sqlCondition)){
				
				query = query + " AND " + sqlCondition;
				
			}
			
			result = existValueInTable(query);
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "existValueInTable", query);
		}
		return result;
	}
	
	/**
	 * You can check if a record exists in a given table
	 * @param query Judgment to validate whether records exist
	 * @return True if the SQL statement casts records
	 * 
	 * @throws QException an exception if they encounter a problem when attempting the operation
	 */
	public Boolean existValueInTable(String query) throws QException{
		
		Boolean result = false;
		
		try {
			
			ResultSet rs = null;
			
			rs = getResultSet(query);
			
			while(rs.next()){
				
				result = true;
			}
			rs.close();
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "existValueInTable", query);
		}
		return result;
	}
	
	/**
	 * Function responsible for building a prepared statement from SQL statements and the parameters of the judgment
	 * 
	 * @param query SQL to prepare the prepared statement
	 * @param param Parameters where the field names, data types and values of each
	 * @param sqlOperationTarget Type of operation destination for prepared SQL statement
	 * @param tableNameTarget Target table for PreparedStatement
	 * @return Returns a SQL (PreparedStatement) statement to be executed
	 * @throws QException throws exception if it fails to build the SQL statement
	 */
	public PreparedStatement getPreparedStatement(String query, ArrayList<SQLParameter> param, SQLOperations sqlOperationTarget, String tableNameTarget) throws QException{
		
		PreparedStatement preparedStatement = null;
		
		try {
			
			preparedStatement = (PreparedStatement) dbConnection.getConnection().prepareStatement(query);
			
			int i = 1;
			
			for (SQLParameter parametroSQL : param) {
				
				if((parametroSQL.getIsMandatory() && parametroSQL.getValue() == null)){
					
					throw new QException(ExceptionType.DatabaseOperation_err,
							className + "-getPreparedStatement", "");
					
				}else{
					
					if((parametroSQL.getValue() != null) || 
							((parametroSQL.getType() == SQLDataType.AutoIncrement_sdt) && (sqlOperationTarget != SQLOperations.Update))){
						
						
						switch (parametroSQL.getType()) {
						
							case String_sdt:
								preparedStatement.setString(i, (String)parametroSQL.getValue());
								break;
							
							case Integer_sdt:
								preparedStatement.setInt(i, (Integer)parametroSQL.getValue());
								break;
								
							case Long_sdt:
								preparedStatement.setLong(i, (Long)parametroSQL.getValue());
								break;
							
							case Decimal_sdt:
								preparedStatement.setDouble(i, (Double)parametroSQL.getValue());
								break;
								
							case Date_sdt:
								Date fecha = (Date)parametroSQL.getValue();
								preparedStatement.setDate(i, fecha);
								break;
								
							case DateTime_sdt:
								Timestamp fechaHora = (Timestamp)parametroSQL.getValue();
								preparedStatement.setTimestamp(i, fechaHora);
								break;
							
							case Boolean_sdt:
								preparedStatement.setBoolean(i, (Boolean)parametroSQL.getValue());
								break;
							case AutoIncrement_sdt:
								
								if(sqlOperationTarget == SQLOperations.Insert){
									
									Long sgteValor = getNextNumberValue(tableNameTarget, 
											parametroSQL.getFieldName(), 
											parametroSQL.getSQLConditionAI());
									
									preparedStatement.setLong(i, sgteValor); 
											
								}else if((sqlOperationTarget == SQLOperations.Update) &&
										(parametroSQL.getValue() != null)){
									preparedStatement.setLong(i, (Long) parametroSQL.getValue());
								}
								
								break;
								
							default:
								break;
						}
						
						i++;
					}
				}
			}
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-" + "-getPreparedStatement");
		}
		return preparedStatement;
	}
	
	/**
	 * Sets the current starting point of the transaction
	 * @throws QException an exception where it informs that the transaction failed to start if you can not start
	 */
	public void beginTransaction() throws QException{
		
		try {
			
			dbConnection.setAutoCommit(false);
			
		    savePoint = dbConnection.setSavepoint();
		    
		    transactionInitiated = true;
		} catch (QException e) {
			throw e;
		}
	}
	
	/**
	 * Make a confirmation in the database
	 */
	public Result commitTransaction(){
		
		Result result = new Result();
		
		try {
			dbConnection.commit();
			
		} catch (QException e) {
			
		} finally{
			
			try {
				
				//The transaction parameters are restored in the database
				dbConnection.setAutoCommit(true);
				savePoint = null;
				transactionInitiated = false;
				
			} catch (QException e2) {
				result.setException(e2);
			}
		}
		return result;
	}
	
	/**
	 * Rollback the transaction
	 * @return Operation result
	 */
	public Result rollBackTransaction(){
		
		Result result = new Result();
		
		try {
		
			if(savePoint != null){

				dbConnection.rollBack(savePoint);
			}else{
				result.setException(new QException(ExceptionType.DatabaseOperation_err,
						className + "-rollBackTransaction"));
			}
		} catch (QException e) {
			result.setException(e);
			
		} finally{
			
			try {

				//The transaction parameters are restored in the database
				dbConnection.setAutoCommit(true);
				savePoint = null;
				transactionInitiated = false;
				
			} catch (QException e2) {
				
				result.setException(e2);
			}
		}
		return result;
	}
	
	
	/**
	 * SQL function that sets the operator for the trim function according to the type of engine database
	 * 
	 * @param field Field name to which it wishes to perform the Trim
	 * @return Trim formatted with the field operator
	 */
	public String getTrimSQL(String field){
		String trimSQL = field;
		switch (getDbEngine()) {
			case MySQL:
				trimSQL = "TRIM(" + field.toString() + ")";
				break;
			default:
				break;
		}
		return trimSQL;
	}

	/**
	 * SQL function that sets the operator for the upper function according to the database engine
	 * @param field Field name to set the sql function
	 * @return Field formated with the sql function
	 */
	public String getUpperSQL(String field){
		String upperSQL = field;
		switch (getDbEngine()) {
			case MySQL:
				upperSQL = "UPPER(" + field.toString() + ")";
				break;
			default:
				break;
		}
		return upperSQL;
	}

	/**
	 * SQL function that sets the operator for the upper function according to the database engine
	 * @param field Field name to set the sql function
	 * @return Field formated with the sql function
	 */
	public String getLowerSQL(String field){
		String lowerSQL = field;
		switch (getDbEngine()) {
			case MySQL:
				lowerSQL = "LOWER(" + field.toString() + ")";
				break;
			default:
				break;
		}
		return lowerSQL;
	}

	/**
	 * @return If a transaction initiated
	 */
	public Boolean isTransactionInitiated() {
		return transactionInitiated;
	}
	
	/**
	 * @param autoCommit If a commit is performed automatically by executing SQL statements
	 */
	public void setAutoCommit(Boolean autoCommit) throws QException{
		
		try {
			
			dbConnection.setAutoCommit(autoCommit);
			
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className + "-setAutoCommit");
		}
		
		
	}
	
	/**
	 * Build a select case query for the database engine assigned
	 * 
	 * @param fieldName Field name that contains the values
	 * @param hashMap Keys stored in the database (By default the keys must to be numeric)
	 * @param alias Values to be mapped
	 * @return Select case string
	 */
	@SuppressWarnings("rawtypes")
	public String buildSelectCase(String fieldName, Map hashMap, String alias){
		return buildSelectCase(fieldName, hashMap.keySet().toArray(), hashMap.values().toArray(), alias, false);
	}
	
	/**
	 * Build a select case query for the database engine assigned
	 * 
	 * @param fieldName Field name that contains the values
	 * @param hashMap Keys stored in the database
	 * @param alias Values to be mapped
	 * @return Select case string
	 */
	@SuppressWarnings("rawtypes")
	public String buildSelectCase(String fieldName, Map hashMap, String alias, Boolean isStringKeys){
		return buildSelectCase(fieldName, hashMap.keySet().toArray(), hashMap.values().toArray(), alias, isStringKeys);
	}
	
	 /**
     * Build a select case query for the database engine assigned
     * 
     * @param fieldName Field name that contains the values
     * @param keys Keys stored in the database
     * @param values Values to be mapped
     * @param alias Alias to set in the query
     * @return Select case string
     */
	public String buildSelectCase(String fieldName, Object[] keys, Object[] values, String alias, Boolean isStringKeys){
		
		String selectQuery = "";
		Result result = new Result();
		
		try {
			if(dbConnection != null){
	        	switch (getDbEngine()) {
					case MySQL:
						selectQuery = buildSelectCaseSQLMySQL(fieldName, keys, values, alias, isStringKeys);
						break;
					default:
						break;
				}
	        }
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.OperationFailed_err, className + "-buildSelectCase"));
		}
        return selectQuery;
    }
	 
    /**
     * Build a select case query for the database engine assigned
     * 
     * @param fieldName Field name that contains the values
     * @param keys Keys stored in the database
     * @param values Values to be mapped
     * @param alias  Alias to set in the query
     * @return Select case string
     */
	private String buildSelectCaseSQLMySQL(String fieldName, Object[] keys, Object[] values, String alias, Boolean isStringKeys){
        	
		String selectQuery = "";
		Result result = new Result();
		
		try {
			
			if(keys.length == values.length){
				
				selectQuery = "CASE " + fieldName;
				
				for (int i = 0; i < keys.length; i++) {
					
					if(keys[i] != null){
						if(!isStringKeys){
							selectQuery += " WHEN " + keys[i].toString() + " THEN '" + values[i] + "'";
						}else{
							selectQuery += " WHEN '" + keys[i].toString() + "' THEN '" + values[i] + "'";
						}
					}
				}
				selectQuery += " ELSE '-' " + " END " + alias;
			}
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.OperationFailed_err, className + "-buildSelectCase"));
		}
		return selectQuery;
	}
	
	/**
	 * Build a concat query from a field names array
	 * @param fieldAlias Alias for the result field
	 * @param fieldNames Fields to concat
	 * @return Concat SQL
	 */
	public String buildConcatSQL(String fieldAlias, String... fieldNames){
		
		String concatQuery = "";
		
		try {
			if(fieldNames != null){
				
				String concatSeparator = "";
				String fieldSeparator = "";
				
				DbEngines dbEngine = getDbEngine();
				for (int i = 0; i < fieldNames.length; i++) {
					
					String fieldName = fieldNames[i];
					if(!TypesValidator.isNullOrEmpty(fieldName)){
						
						if(TypesValidator.isNullOrEmpty(concatQuery)){
							switch (dbEngine) {
								case MySQL:
									concatQuery = " CONCAT(";
									break;
								default:
									break;
								}
						}
						concatQuery = concatQuery + fieldSeparator + concatSeparator + fieldSeparator + fieldName;
						switch (dbEngine) {
							case MySQL:
								fieldSeparator = ",";
								concatSeparator = "' '";
								break;
							default:
								break;
							}
						}
					}
				if(!TypesValidator.isNullOrEmpty(concatQuery)){
					if(!TypesValidator.isNullOrEmpty(fieldAlias)){
						concatQuery = concatQuery + " ) AS " + fieldAlias;
					}else{
						concatQuery = concatQuery + " )";
					}
				}
			}
		} catch (Exception e) {
			new QException(e, ExceptionType.OperationFailed_err, className + "-buildConcatSQL");
		}
		return concatQuery;
	}
	
	/**
	 * Internal class that contains information on the outcome of the database operations 
	 * 
	 * @author jholguin
	 * @since 16/12/2015
	 *
	 */
	public class InfoDbOperation{
		
		private String query = "";
		private Integer updatedRows = 0;
		private Long lastId = 0L;
		
		public InfoDbOperation() {
			
		}

		/**
		 * @return Last identifier inserted to the database
		 */
		public Long getLastId() {
			return lastId;
		}

		/**
		 * @param id Last identifier inserted to the database
		 */
		public void setLastId(Long id) {
			this.lastId = id;
		}

		/**
		 * @return SQL statement executed in the operation
		 */
		public String getQuery() {
			return query;
		}

		/**
		 * @param value SQL statement executed in the operation
		 */
		public void setSentenciaSQL(String value) {
			this.query = value;
		}

		/**
		 * @return Total rows updated with SQL operation Executed
		 */
		public Integer getUpdatedRows() {
			return updatedRows;
		}

		/**
		 * @param value Total rows updated with SQL operation Executed
		 */
		public void setUpdatedRows(Integer value) {
			this.updatedRows = value;
		}
	}

	/**
	 * Gets a camelcase name from a string removing the table owner prefix
	 * @param s
	 * @param removePrefix
	 * @return
	 */
	public String getCamelCaseName(String s, boolean removePrefix) {
		if(!s.contains("_")){
			return s;
		}
		String[] vals = s.split("_");
		int base = 0;
		if(removePrefix){
			base = 1;
		}
		String r = vals[base];
		for(int i=base+1;i<vals.length;i++){
			r += vals[i].substring(0,1).toUpperCase()+vals[i].substring(1);
		}
		return r;
	}

	/**
	 * Gets a camel case name from a string
	 * @param s
	 * @return
	 */
	public String getCamelCaseName(String s) {
		return getCamelCaseName(s, true);
	}

	/**
	 * Gets a list of values in dictionary form (key, value) from a database query
	 * @param query
	 * @return
	 * @throws QException
	 */
	public List<Map<String, Object>> getMapArrayFromQuery(String query) throws QException {

		List<Map<String, Object>> res = new ArrayList<Map<String,Object>>();
		ResultSetMetaData rsmd;
		String name = "none";
		String colname = "undefined";
		String label = "no_label";

		try {

			ResultSet rs = null;

			try {
				rs = getResultSet(query);
			} catch (QException e) {
				throw e;
			}
			while(rs.next()){
				rsmd = (ResultSetMetaData) rs.getMetaData();
				int colCount = rsmd.getColumnCount();
				Map<String, Object> r = new HashMap<String, Object>();
				for(int i=1;i<=colCount;i++){
					colname = rsmd.getColumnName(i);
					label = rsmd.getColumnLabel(i);
					if(!colname.equals(label)){
						name = getCamelCaseName(label, false);
					} else {
						name = getCamelCaseName(colname, true);
					}
					r.put(name, rs.getObject(i));
				}
				res.add(r);
			}

			closeResultSet(rs);
		} catch (Exception e) {
			throw new QException(e, ExceptionType.DatabaseOperation_err, className+" - getMapArrayFromQuery");
		}
		return res;
	}


	/**
	 * Close a resultSet and sets = null
	 * @param rs Resultset to close
	 */
	protected void closeResultSet(ResultSet rs){

		try {
			if(rs != null){
				rs.close();
			}
			rs = null;
		} catch (Exception e) {
			new QException(e, ExceptionType.OperationFailed_err, className + "-closeResultSet");
		}
	}

}
