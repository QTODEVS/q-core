package com.quamto.core;

import java.util.Locale;

import com.quamto.core.QException.ExceptionType;
import com.quamto.db.DbOperator.InfoDbOperation;

/**
 * Class used to collect information resulting from an operation
 * 
 * @author jholguin
 * @since 14/05/2015
 *
 */
public class Result {
	
	private Boolean successful = null;
	private QException exception = null;
	private InfoDbOperation infoSQL = null;
	private IUserMessages userMessage = null;
	private String stringMessage = null;
	private Boolean isUnauthorizedRequest = false;
	private Object payload = null;
	private Credential credential = null;

	
	public Result(){
		this(true, (QException)null);
	}
	
	/**
	 * 
	 * @param isSuccessful It defines if the operation had a successful outcome
	 */
	public Result(Boolean isSuccessful) {
		this(isSuccessful, (QException)null);
	}
	
	/**
	 * 
	 * @param exception Exception information
	 */
	public Result(QException exception) {
		this(false, exception);
	}
	
	/**
	 * @param e Java Exception information
	 */
	public Result(Exception e){
		this(false, new QException(e));
	}
	
	/**
	 * @param e Java Exception information
	 * @param source Source where the exception was throw
	 */
	public Result(Exception e, String source){
		this(false, new QException(e, ExceptionType.NotIdentified_err, source));
	}
	
	/**
	 * 
	 * @param userMessage String message for the user
	 */
	public Result(IUserMessages userMessage) {
		this(true, (QException)null, userMessage);
	}
	
	/**
	 * 
	 * @param isSuccessful It defines if the operation had a successful outcome
	 * @param userMessage String message for the user
	 */
	public Result(Boolean isSuccessful, IUserMessages userMessage) {
		this(isSuccessful, (QException)null, userMessage);
	}
	
	/**
	 * 
	 * @param isSuccessful It defines if the operation had a successful outcome
	 * @param exception Sets the exception resulting from the unsuccessful operation
	 */
	public Result(Boolean isSuccessful, QException exception) {
		this(isSuccessful, exception, null);
	}
	
	/**
	 * 
	 * @param isSuccessful It defines if the operation had a successful outcome
	 * @param exception Sets the exception resulting from the unsuccessful operation
	 * @param userMessage String message for the user
	 */
	public Result(Boolean isSuccessful, QException exception, IUserMessages userMessage) {
		
		this.successful = isSuccessful;
		this.exception = exception;
		this.userMessage = userMessage;
		
		if(userMessage != null){
			this.stringMessage = userMessage.getStringMessage();
		}
	}

	/**
	 * @return If the operation was successful
	 */
	public Boolean isSuccessful() {
		return this.successful;
	}

	/**
	 * @param value If the operation was successful
	 */
	public void setSuccessful(Boolean value) {
		this.successful = value;
	}

	/**
	 * @param exception Sets the exception occurred in the operation therefore 
	 * set as the operation successful
	 */
	public void setException(QException exception) {
		this.exception = exception;
		if(this.exception != null){
			if(this.exception.getType() != ExceptionType.Nothing_err){
				this.successful = false;
			}
		}else{
			this.successful = true;
		}
	}

	
	/**
	 * @return Exception occurred in the operation
	 */
	public QException getException() {
		return exception;
	}

	/**
	 * @return Information about the SQL transaction executed in the database
	 */
	public InfoDbOperation getInfoSQL() {
		return infoSQL;
	}

	/**
	 * @param infoSQL Information about the SQL transaction executed in the database
	 */
	public void setInfoSQL(InfoDbOperation infoSQL) {
		this.infoSQL = infoSQL;
	}
	
	/**
	 * @param message Message information
	 */
	public void setMessage(IUserMessages message) {
		userMessage = message;
		if(message != null){
			this.stringMessage = message.getStringMessage();
		}
	}
	
	/**
	 * @return String message of the operation result
	 */
	public String getStringMessage() {
		return stringMessage;
	}

	/**
	 * @return If the operation request is unauthorized
	 */
	public Boolean isUnauthorizedRequest() {
		return isUnauthorizedRequest;
	}

	/**
	 * @param value If the operation request is unauthorized
	 */
	public void isUnauthorizedRequest(Boolean value) {
		this.isUnauthorizedRequest = value;
	}

	public Result translateLocaleMessages(){
		Result result = this;
		try {
			Locale locale = null;
			
			if(credential != null){
				locale = credential.getLocale();
			}
			if(locale == null){
				locale = Locale.getDefault();			
			}
			result = translateLocaleMessages(result, locale);
		} catch (Exception e) {
			System.out.println("Can't translate result " + e.getMessage());
		}
		return result;
	}
	
	/**
	 * Translate the user messages to the geographical position
	 * @param locale User geographical position
	 * @return User messages translated
	 */
	public Result translateLocaleMessages(Locale locale){
		return translateLocaleMessages(this, locale);
	}
	
	/**
	 *  Translate the user messages to the geographical position
	 * @param result Result that contains message information
	 * @param locale User geographical position
	 * @return User messages translated
	 */
	public Result translateLocaleMessages(Result result, Locale locale){
		
		try {
			
			if(result.exception != null){
				result.exception.translateLocaleMessages(locale);
			}
			
			if(userMessage != null){
				this.stringMessage = userMessage.getStringMessage(locale);
			}
		} catch (Exception e) {
			QException.printLogException(e);
		}
		return result;
	}

	/**
	 * @return Result data from the operation
	 */
	public Object getPayload() {
		return payload;
	}

	/**
	 * @param value Result data from the operation
	 */
	public void setPayload(Object value) {
		this.payload = value;
	}

	/**
	 * @return Credentials for the operation
	 */
	public Credential getCredential() {
		return credential;
	}

	/**
	 * @param value Credentials for the operation
	 */
	public void setCredentials(Credential value) {
		this.credential = value;
	}
}
