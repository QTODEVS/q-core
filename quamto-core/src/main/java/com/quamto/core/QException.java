package com.quamto.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.quamto.util.TypesValidator;

/**
 * Class used to manage the application exceptions
 * @author jholguin
 * @since 16/12/2015
 *
 */
public class QException extends Exception {
	
	/**
	 * Exceptions types defined
	 * 
	 * @author jholguin
	 * @since 16/12/2015
	 */
	public enum ExceptionType{
		Nothing_err,
		NotIdentified_err,
		DatabaseOperation_err,
		OperationNotAllowed_err,
		IncompleteParameters_err,
		DataIntegrity_err,
		ValidationData_err,
		LoadData_err,
		OperationFailed_err,
		InvalidAuthentication_err,
		InvalidIdentifier_err;
	}
	
	
	/**
	 * Attributes that contain additional information about the problem occurred
	 */
	private static final long serialVersionUID = 1L;
	private String details = "";
	private String source = "";
	private ExceptionType type = ExceptionType.NotIdentified_err;
	private String aditionalInformation = "";
	private Boolean printedLog = false;



	private String typeDescription = null;
	private List<IUserMessages> userMessages = null; 

	public QException(){
		InitializateExceptionInfo(ExceptionType.Nothing_err, "", "", "");
	}
	
	/**
	 * Exception constructor
	 * 
	 * @param type Exception type
	 */
	public QException(ExceptionType type){
		InitializateExceptionInfo(type, "", "", "");
	}
	
	/**
	 * Exception constructor
	 * 
	 * @param type Exception type
	 * @param source Source from which the information 
	 * 
	 */
	public QException(ExceptionType type, String source){
		InitializateExceptionInfo(type, "", source, "");
	}

	public QException(String message){
		InitializateExceptionInfo(ExceptionType.OperationFailed_err, message, "", "");
	}

	public QException(String message, ExceptionType type){
		InitializateExceptionInfo(type, message, "", "");
	}

	public QException(String message, ExceptionType type, String source){
		InitializateExceptionInfo(type, message, source, "");
	}

	/**
	 * Exception constructor
	 * 
	 * @param type Exception type
	 * @param source Source from which the information 
	 * @param aditionalInformation Additional information to complete the exception 
	 */
	public QException(ExceptionType type, String source, String aditionalInformation){
		InitializateExceptionInfo(type, "", source, aditionalInformation);
	}
	
	public QException(IUserMessages userMessage){
		this(ExceptionType.NotIdentified_err, userMessage);
	}
	
	public QException(ExceptionType type, IUserMessages userMessage){
		InitializateExceptionInfo(type, userMessage, "", "");
	}
	
	public QException(ExceptionType type, IUserMessages userMessage, String source){
		InitializateExceptionInfo(type, userMessage, source, "");
	}
	
	public QException(ExceptionType type, IUserMessages userMessage, String source, String additionalInformation){
		InitializateExceptionInfo(type, userMessage, source, additionalInformation);
	}
	
	public QException(ExceptionType type, List<IUserMessages> userMessages){
		InitializateExceptionInfo(type, userMessages, "", "");
	}
	
	public QException(ExceptionType tipo, List<IUserMessages> userMessages, String origen, String infoAdicional){
		InitializateExceptionInfo(tipo, userMessages, "", "");
	}
	
	/**
	 * Exepcion constructor initializer which establishes the exception 
	 * captured directly from a try catch block, in order to customize 
	 * the handling of internal exceptions to the application
	 * 
	 * @param e Direct Java exception in order to initialize the custom exception
	 */
	public QException(Exception e){
		this(e, ExceptionType.NotIdentified_err, "", "");
	}
	
	/**
	 * Exepcion constructor initializer which establishes the exception 
	 * captured directly from a try catch block, in order to customize 
	 * the handling of internal exceptions to the application
	 * 
	 * @param type Exception type
	 * @param e Direct Java exception in order to initialize the custom exception
	 */
	public QException(ExceptionType type, Exception e){
		this(e, type, "");
	}
	
	/**
	 * Exepcion constructor initializer which establishes the exception 
	 * captured directly from a try catch block, in order to customize 
	 * the handling of internal exceptions to the application
	 * 
	 * @param type Exception type
	 * @param e Direct Java exception in order to initialize the custom exception
	 * @param source Source from which the information 
	 */
	public QException(Exception e, ExceptionType type, String source){
		this(e, type, source, "");
	}
	
	/**
	 * Exepcion constructor initializer which establishes the exception 
	 * captured directly from a try catch block, in order to customize 
	 * the handling of internal exceptions to the application
	 * 
	 * @param type Exception type
	 * @param e Direct Java exception in order to initialize the custom exception
	 * @param source Source from which the information
	 * @param aditionalInformation Additional information to complete the exception 
	 */
	public QException(Exception e, ExceptionType type, String source, String aditionalInformation){
		
		String details = "";
		
		try {
			details = e.getMessage();
			
			if(details == null){
				details = e.toString();
			}
		} catch (Exception e2) {
			details = "Details is not defined!";
		}
		InitializateExceptionInfo(type, details, source, aditionalInformation);
	}


	public void setDescription(String description){
		this.details = description;
	}

	/**
	 * Gets error type description
	 * @return String with the error type description
	 */
	public String getTypeDescription() {
		return typeDescription;
	}

	/**
	 * Sets the error type description
	 * @param typeDescription Description of the type error
	 */
	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}

	/**
	 * 
	 * @return Exception description
	 */
	public String getDescription() {
		return details;
	}

	/**
	 * @return If the exception information was printed in the console log
	 */
	public Boolean getPrintedLog() {
		return printedLog;
	}

	/**
	 * @param printedLog If the exception information was printed in the console log
	 */
	public void setPrintedLog(Boolean printedLog) {
		this.printedLog = printedLog;
	}

	/**
	 * 
	 * @return Source of where the exception originated
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @return Exception Type
	 */
	public ExceptionType getType() {
		return type;
	}
	
	/**
	 * @return Additional important information that can guide the problem occurred
	 */
	public String getAditionalInformation() {
		return aditionalInformation;
	}

	/**
	 * @param value Additional important information that can guide the problem occurred
	 */
	public void setAditionalInformation(String value) {
		this.aditionalInformation = value;
	}

	/**
	 * GetMessage overload method that returns the error message
	 * @return The exception message
	 */
	@Override
	public String getMessage() {
		return this.details;
	}
	
	/**
	 * @return User messages assigned to the exception
	 */
	public List<IUserMessages> getUserMessages() {
		return userMessages;
	}

	/**
	 * @param userMessages User messages assigned to the exception
	 */
	public void setUserMessages(List<IUserMessages> userMessages) {
		this.userMessages = userMessages;
	}
	
	/**
	 * @return String exception summary
	 */
	public String getExceptionSummary(){
		String summary = "";
		String typeDescription = "Not identified";
		
		if(this.typeDescription != null){
			typeDescription = this.typeDescription;
		}
		
		summary += "Type: " + typeDescription + "\n";
		summary += "Details: " + details + "\n";
		
		if(!TypesValidator.isNullOrEmpty(source)){
			summary += "Source: " + source + "\n";
		}
		if(!TypesValidator.isNullOrEmpty(aditionalInformation)){
			summary += "Aditional Info:" + this.aditionalInformation + "\n";
		}
		return summary;
	}
	
	/**
	 *  Translate the user messages to the geographical position
	 * @param locale User geographical position
	 */
	public void translateLocaleMessages(Locale locale){
		this.InitializateExceptionInfo(this.type, this.userMessages, this.source, this.aditionalInformation, locale);
	}
	
	/**
	 * Initializate the exception information
	 * 
	 * @param type Exception type
	 * @param details Details that contains the exception information
	 * @param source Source from which the information 
	 * @param aditionalInfo Complementary information
	 */
	private void InitializateExceptionInfo(ExceptionType type, String details, String source, String aditionalInfo){
		
		this.details = details;
		this.source = source;
		this.type = type;
		this.aditionalInformation = aditionalInfo;
		
		switch (type) {
			case NotIdentified_err:
				typeDescription = "Error in the application unrated";
				break;
			case OperationNotAllowed_err:
				typeDescription = "Attempted Operation not permitted";
				break;
			case DatabaseOperation_err:
				typeDescription = "Error when trying to complete an operation in the database";
				break;
			case IncompleteParameters_err:
				typeDescription = "Insufficient information to complete the operation";
				break;
			case DataIntegrity_err:
				typeDescription = "Problems of data integrity";
				break;
			case LoadData_err:
				typeDescription = "Problems when trying to load information";
				break;
			case ValidationData_err:
				typeDescription = "Errors in the internal data validation";
				break;
			case OperationFailed_err:
				typeDescription = "An attempt the current operation";
				break;
			case InvalidAuthentication_err:
				typeDescription = "Currently it is not authenticated to continue the operation";
				break;
			default:
				break;
		}
		if(!printedLog){
			printLogException(this);
		}
	}
	
	/**
	 * Print the exception message in the console log
	 * @param e Exception information
	 */
	public static void printLogException(Exception e){
		try {
			if(e != null){
				QException qException = new QException(e);
				printLogException(qException);
				qException = null;
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
	
	/**
	 * Print the exception message in the console log
	 * @param e Exception information
	 */
	public static void printTestLogException(Exception e){
		try {
			if(e != null){
				QException qException = new QException(e);
				System.out.println("==============");
				System.out.println("TEST EXCEPTION");
				System.out.println("==============");
				printLogException(qException);
				qException = null;
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
	
	/**
	 * Print the exception message in the console log
	 * @param e Exception information
	 */
	public static void printLogException(QException e){
		try {
			if(e != null){
				System.out.println(e.getExceptionSummary());
				e.setPrintedLog(true);
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
	
	/**
	 * Initializate the exception information
	 * 
	 * @param type Exception type
	 * @param userMessage User message object that contains the message code
	 * @param source Source from which the information 
	 * @param aditionalInfo Complementary information
	 */
	private void InitializateExceptionInfo(ExceptionType type, IUserMessages userMessage, String source, String aditionalInfo){
		
		List<IUserMessages> userMessages = new ArrayList<IUserMessages>();
		
		userMessages.add(userMessage);
		this.InitializateExceptionInfo(type, userMessages, source, aditionalInfo, Locale.getDefault());
	}
	
	/**
	 * Initializate the exception information
	 * 
	 * @param type Exception type
	 * @param userMessages User message object that contains the message code
	 * @param source Source from which the information 
	 * @param aditionalInfo Complementary information
	 */
	private void InitializateExceptionInfo(ExceptionType type, List<IUserMessages> userMessages, String source, String aditionalInfo){
		this.InitializateExceptionInfo(type, userMessages, source, aditionalInfo, Locale.getDefault());
	}	
	
	/**
	 * Initializate the exception information
	 * 
	 * @param type Exception type
	 * @param userMessages User message object that contains the message code
	 * @param source Source from which the information 
	 * @param aditionalInfo Complementary information
	 */
	private void InitializateExceptionInfo(ExceptionType type, List<IUserMessages> userMessages, String source, String aditionalInfo, Locale locale){
		
		if(userMessages != null){
			
			String details = "";
			
			this.userMessages = userMessages;
			
			for (int i = 0; i < userMessages.toArray().length; i++) {
				
				IUserMessages userMessage = userMessages.get(0);
				
				if(userMessage != null){
					details += userMessage.getStringMessage(locale);
				}
			}
			
			InitializateExceptionInfo(type, details, source, aditionalInfo);
		}
	}

}

