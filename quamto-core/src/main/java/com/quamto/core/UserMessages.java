package com.quamto.core;

import java.util.Locale;
import java.util.ResourceBundle;

import com.quamto.core.IUserMessages;

/**
 * Class used to manage the string messages for the application
 * @author jholguin
 * @since 16/12/2015
 *
 */
public class UserMessages implements IUserMessages {

	private String messageCode = null;
	private String messageModule = null;
	private Object[] messageArgs = null;
	private ResourceBundle messageResource = null;
	private Locale messageLocale = null;
	
	
	private String messageFileName = "";
	
	public enum ModuleMessages{
		ErrorMessages("error"),
		EnumeratorMessages("enum");
		
		
		String stringKey = null;
		
		private ModuleMessages(String key){
			stringKey = key;
		}
		
		/**
		 * Gets the string Key for the enum value
		 * @return String key
		 */
		private String getKey(){
			return stringKey;
		}
		
		
	}
	 
	public enum CommonMessages{
		RecordAssociated(100);
		
		private Integer intValue = null;
		
		private CommonMessages(Integer value){
			intValue = value;
		}
		
		/** 
		 * @return Gets the integer value
		 */
		public Integer getInt(){
			return intValue;
		}
	}
	
	public enum ErrorMessages{
		NotIdentifiedError(100),
		DatabaseOperationError(101),
		NotAllowedOperationError(102),
		IncompleteParametersError(103),
		DataIntegrityError(104),
		DataValidationError(105),
		DataLoadError(106),
		OperationFailError(107),
		InvalidAuthenticationError(108),
		InvalidIdentifierError(109),
		CantBuildStatement(110),
		InvalidApplicationPath(111),
		InvalidUserInformation(112),
		ExpiredSession(113),
		FileTypeProhibited(114),
		FileExceedsSize(115),
		InvalidFileOwner(116),
		InvalidFileInformation(117),
		IncorrectObjectInitialization(118),
		InvalidDirectoryPath(119),
		InvalidSMTPConfiguration(120),
		TransactionNotIniated(121),
		EmailAlreadyRegistered(122),
		IncorrectPassword(123),
		UnregisterUser(124),
		SomeOperationsFailed(125),
		LoadApplicationConfiguration(126),
		DuplicatedRelation(127),
		AdministrationNotAllowed(128),
		OperationRestricted(129),
		ProjectWithoutSecuritySchema(130),

		NoModuleRelatedChecklist(131),
		AuditedItemAlreadyExits(132);
		
		private Integer intValue = null;
		
		private ErrorMessages(Integer value){
			intValue = value;
		}
		
		/** 
		 * @return Gets the integer value
		 */
		public Integer getInt(){
			return intValue;
		}
	}
	
	/**
	 * Error message constructor
	 * @param message Message code
	 */
	public UserMessages(ErrorMessages message) {
		this(message, (Object)null);
	}
	
	/**
	 * Error message constructor
	 * @param message Message code
	 */
	public UserMessages(ErrorMessages message, Object... messageArgs) {
		InitializateUserMessage(ModuleMessages.ErrorMessages, message.getInt().toString(), messageArgs);
	}
	
	/**
	 * Module message constructor that initialized the message 
	 * @param moduleMessages Modulo owner of the message
	 */
	public UserMessages(ModuleMessages moduleMessages){
		InitializateUserMessage(moduleMessages, "", null);
	}
	
	/**
	 * @return Messages string code
	 */
	public String getMessageCode() {
		return messageCode;
	}

	/**
	 * @return Module string key
	 */
	public String getMessageModule() {
		return messageModule;
	}

	/**
	 * @return Messages arguments used to complete the message information
	 */
	public Object[] getMessageArgs() {
		return messageArgs;
	}

	/**
	 * Generic message constructor
	 * @param messageModule Message module owner 
	 * @param messageCode Message code 
	 */
	private void InitializateUserMessage(ModuleMessages messageModule, String messageCode, Object messageArgs[]){
		
		messageFileName = "user-messages";
		if(messageModule == ModuleMessages.EnumeratorMessages){
			messageFileName = "enum-descriptions";
		}
		
		this.messageModule = messageModule.getKey();
		this.messageCode = messageCode;
		this.messageArgs = messageArgs;
		this.messageLocale = Locale.getDefault();
		this.messageResource = ResourceBundle.getBundle(messageFileName, this.messageLocale);
	}
	
	/**
	 * Gets the string message according to locale default info
	 * @return String message 
	 */
	public String getStringMessage(){
		return getStringMessage(this.messageResource, this.getMessageModule(), this.getMessageCode());
	}
	
	/**
	 * Gets the string message according to locale default info
	 * @param enumName Enumeration name
	 * @param enumCode Enumeration code
	 * @return String message 
	 */
	public String getEnumeratorDescription(String enumName, Integer enumCode){
		return getStringMessage(this.messageResource, this.messageModule, enumName + "." + enumCode.toString());
	}
	
	/**
	 * Gets the string message according to locale info
	 * @param locale Localization information to get the message
	 * @return String message 
	 */
	public String getStringMessage(Locale locale){
		return getStringMessage(locale, this);
	}
	
	/**
	 * Gets the string message according to locale info
	 * @param locale Localization information to get the message
	 * @param userMessage User message information
	 * @return String message 
	 */
	public String getStringMessage(Locale locale, UserMessages userMessage){
		
		String message = "";
		
		try {
			
			if(locale == null){
				locale = Locale.getDefault();
			}
			ResourceBundle kongMessages = ResourceBundle.getBundle(messageFileName, locale);
			
			message = getStringMessage(kongMessages, userMessage.getMessageModule(), userMessage.getMessageCode());
			
			kongMessages = null;
		} catch (Exception e) {
			QException.printLogException(e);
		}
		return message;
	}
	
	/**
	 * Gets the string message according to locale info
	 * @param messageCode message code
	 * @param messageModule message module owner of the message
	 * @param messagesResource Resource bundle that contains the messages
	 * @return String message 
	 */
	public String getStringMessage(ResourceBundle messagesResource, String messageModule, String messageCode){
		
		String message = "";
		
		try {
			if(messagesResource != null){
				
				try {
					message = messagesResource.getString(messageModule + "." + messageCode);
					
					if(messageArgs != null){
						message = String.format(message, messageArgs);
					}
				} catch (Exception e) {
					message = "Message -" + messageModule.toString() + "-"+ messageCode;
				}
			}
		} catch (Exception e) {
			QException.printLogException(e);
		}
		return message;
	}
	
	
}
