package com.quamto.core;

import java.util.Locale;

/**
 * Contains the credentials and information to execute a operation
 * @author jholguin
 * @since 07/06/2016
 *
 */
public class Credential {

	private String user = "";
	private String token = "";
	private String localeID = "";
	
	public Credential() {
		this(null, null);
	}
	
	public Credential(String user, String token) {
		this.user = user;
		this.token = token;
	}
	
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the localeID
	 */
	public String getLocaleID() {
		return localeID;
	}

	/**
	 * @param localeID the localeID to set
	 */
	public void setLocaleID(String localeID) {
		this.localeID = localeID;
	}
	
	/**
	 * @return Gets the locale information
	 */
	public Locale getLocale(){
		return null;
	}
}
