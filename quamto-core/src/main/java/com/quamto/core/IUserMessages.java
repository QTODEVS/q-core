package com.quamto.core;

import java.util.Locale;
import java.util.ResourceBundle;


/**
 * Interface that contains the generic methods to be implemented
 * in the user messages classes
 * 
 * @author jholguin
 * @since 16/12/2015
 *
 */
public interface IUserMessages {
	
	public String getMessageCode();
	public String getMessageModule();
	public Object[] getMessageArgs();
	public String getStringMessage();
	public String getStringMessage(Locale locale);
	public String getEnumeratorDescription(String enumName, Integer enumCode);
	public String getStringMessage(ResourceBundle messagesResource, String messageModule, String messageCode);
}
