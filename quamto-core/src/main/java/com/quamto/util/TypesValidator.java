package com.quamto.util;

import java.text.SimpleDateFormat;
import java.util.Date;



/**
 * Class used for validation of data types
 * 
 * @author jholguin
 * @since 19/05/2015
 *
 */
public abstract class TypesValidator {
	
	
	/**
	 * Allows validate a given value of a date type String
	 * @param dateValue Date String to be validated
	 * @return True if the date is correct, false otherwise
	 */
	public static boolean validateDate(String dateValue){
		return validateDate(dateValue, "dd/MM/yyyy");
	}
	
	/**
	 * Allows validate a given value of a date type String
	 * @param dateValue Date to validate
	 * @param format Format to validate the date
	 * @return True if the date is correct, false otherwise
	 */
	public static boolean validateDate(String dateValue, String format){
		
		Boolean validDate = true;
		
		if(dateValue != null){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			dateFormat.setLenient(false);
		
			try {	
				@SuppressWarnings("unused")
				Date fecha = dateFormat.parse(dateValue);
			} catch (Exception e) {	
				validDate = false;	
			}
		}else {
			validDate = false;
		}
		return validDate;
	}
	
	/**
	 * Validates whether a string is null or empty
	 * @param s String to validate
	 * @return True if the string is null otherwise false
	 */
	public static boolean isNullOrEmpty(String s){
		
		boolean result = false;
		
		if(s == null){
			result = true;
		}else{
			result = s.isEmpty();
		}
		return result;
	}
	
	/**
	 * Validate if a string is a numeric value
	 * @param s String to validate
	 * @return True if the string is a numeric value, false otherwise
	 */
	public static boolean isNumeric(String s){
		
		boolean result = false;
	    try{
	    	
	    	if(s != null){
	    		@SuppressWarnings("unused")
				double y = Double.parseDouble( s );
		        
		        result = true;
	    	}
	    }
	    catch( NumberFormatException err ){
	    	result = false;
	    }
	    return result;
	}
	
}
