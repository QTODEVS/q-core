package com.quamto.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Allows get data types with diferent formats
 * 
 * @author jholguin
 * @since 09/09/2015
 *
 */
public class DataTypesFormat {

	
	/**
	 * Convert a timestamp to String with the format dd/MM/yyyy hh:mm:ss
	 * @param dateTime Date to convert
	 * @return Datetime string formated
	 */
	public static String TimestampToString(Timestamp dateTime){
		return TimestampToString(dateTime, getDateTimeFormat());
	}
	
	/**
	 * Convert a timestamp to String with the given format
	 * @param dateTime Date to convert
	 * @return Datetime string formated
	 */
	public static String TimestampToString(Timestamp dateTime, String dateTimeFormat){
		return new SimpleDateFormat(dateTimeFormat).format(dateTime);
	}
	
	/**
	 * @return Gets the datetime string format by default dd/MM/yyyy hh:mm:ss
	 */
	public static String getDateTimeFormat(){
		return "dd/MM/yyyy hh:mm";
	}
	
}
