package com.quamto.notifications;

/**
 * Class that contains the SMTP configuration
 * for the email notifications
 * 
 * @author jholguin
 * @since 16/12/2015
 *
 */
public class SMTPConfig {

	/**
	 * Class attributes
	 */
	private String password;
	private String mailSender;
	private String mailPrefix;
	private String host;
	private String port;
	private String user;
	
	/**
	 * @param port Port connection to the SMTP server
	 */
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * @return User authentication with the SMTP server
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param value User authentication with the SMTP server
	 */
	public void setUser(String value) {
		this.user = value;
	}
	/**
	 * @return SMTP authentication password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param value SMTP authentication password
	 */
	public void setPassword(String value) {
		this.password = value;
	}
	/**
	 * @return Mail through which mails are answered notifications
	 */
	public String getMailSender() {
		return mailSender;
	}
	/**
	 * @param value Mail through which mails are answered notifications
	 */
	public void setMailSender(String value) {
		this.mailSender = value;
	}
	/**
	 * @return Prefix header that will take the mail notifications
	 */
	public String getMailPrefix() {
		return mailPrefix;
	}
	/**
	 * @param value Prefix header that will take the mail notifications
	 */
	public void setMailPrefix(String value) {
		this.mailPrefix = value;
	}
	/**
	 * @return Route SMTP server
	 */
	public String getHost() {
		return host;
	}
	/**
	 * @param value Route SMTP server
	 */
	public void setHost(String value) {
		this.host = value;
	}
	/**
	 * @return Port connection to the SMTP server
	 */
	public String getPort() {
		return port;
	}
	
	
}
