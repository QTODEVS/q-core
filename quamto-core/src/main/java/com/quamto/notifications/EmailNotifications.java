package com.quamto.notifications;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.quamto.core.QException;
import com.quamto.core.QException.ExceptionType;
import com.quamto.core.Result;

public class EmailNotifications {
	
	String className = "";
	SMTPConfig SMTPConfig = null;
	
	/**
	 * Class constructor 
	 * @param SMTPConfig SMTP configuration
	 */
	public EmailNotifications(SMTPConfig SMTPConfig) {
		this.SMTPConfig = SMTPConfig;
	}
	
	/**
	 * Sends a notification via email
	 * @param notification Notification to send
	 * @return Operation result
	 */
	public Result sendNotification(Notification notification){
		
		Result result = new Result();
		
		try {
			
			if(SMTPConfig != null){
				
				Properties props = new Properties();
				Authenticator authenticator = new Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(SMTPConfig.getUser(), SMTPConfig.getPassword());
					}
				  };

				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", SMTPConfig.getHost());
				props.put("mail.smtp.port", SMTPConfig.getPort());
				 
				Session session = Session.getInstance(props, authenticator);
				Message message = new MimeMessage(session);
				
				message.setFrom(new InternetAddress(SMTPConfig.getMailSender()));
				message.setRecipients(Message.RecipientType.TO, 
						InternetAddress.parse(notification.emailDestination));
				message.setSubject(SMTPConfig.getMailPrefix() + ":" + notification.getSubject());
				message.setText("Hola " + notification.nameDestination + ","
					+ "\n\n " + notification.message
					+ "\n\n Atentamente"
					+ "\n\n Quality Management Team (Quamto)");

				Transport.send(message);
			}else{
				result.setException(new QException(ExceptionType.IncompleteParameters_err, 
					   className + "sendNotification"));
			}
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.OperationFailed_err, className + "-sendNotification"));
		}
		return result;
	}
	
}
