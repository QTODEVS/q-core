package com.quamto.notifications;

public class Notification {

	/**
	 * Attributes class
	 */
	String senderName = null;
	String nameDestination = null;
	String emailDestination = null;
	String message = null;
	String subject = null;
	
	/**
	 * @return Sender name of the notification
	 */
	public String getSenderName() {
		return senderName;
	}
	/**
	 * @param senderName Sender name of the notification
	 */
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	/**
	 * @return Email destination
	 */
	public String getEmailDestination() {
		return emailDestination;
	}
	/**
	 * @param emailDestination Email destination
	 */
	public void setEmailDestination(String emailDestination) {
		this.emailDestination = emailDestination;
	}
	/**
	 * @return Message body of the notification
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message Message body of the notification
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return Message subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject Message subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return Name of the destination people
	 */
	public String getNameDestination() {
		return nameDestination;
	}
	/**
	 * @param nameDestination Name of the destination people
	 */
	public void setNameDestination(String nameDestination) {
		this.nameDestination = nameDestination;
	}
	
	
}
