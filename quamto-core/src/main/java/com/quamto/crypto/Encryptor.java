package com.quamto.crypto;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.quamto.core.QException;
import com.quamto.core.QException.ExceptionType;
import com.quamto.util.TypesValidator;


/**
 * Class used to the cryptography operations
 * @author jholguin
 * @since 21/01/2016
 *
 */
public class Encryptor {
	
	private static final String cryptoCharSet = "UTF-8";
	private static final String cryptoAlgoritm = "AES";
	private static final String cryptoTransformation = "AES/CBC/PKCS5PADDING";
	
	/**
	 * Encrypt a String value with AES Standard
	 * @param key secure Key to encrypt the value
	 * @param initVector IvParameter Specification
	 * @param valueToEncrypt Value to Encrypt
	 * @return Value Encrypted
	 * @throws QException Launch exception when something fails
	 */
	public static String encrypt(String key, String initVector, String valueToEncrypt) throws QException{
		
		String valueEncrypted = null;
		
		try {
			
			if(!TypesValidator.isNullOrEmpty(valueToEncrypt)){
				
				IvParameterSpec ivParam = new IvParameterSpec(initVector.getBytes(cryptoCharSet));
				SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(cryptoCharSet), cryptoAlgoritm);
				
				Cipher cipher = Cipher.getInstance(cryptoTransformation);
				cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParam);
				
				byte[] bytesEncrypted = cipher.doFinal(valueToEncrypt.getBytes());
				
				valueEncrypted = Base64.encodeBase64String(bytesEncrypted);

				ivParam = null;
				secretKey = null;
				cipher = null;
				bytesEncrypted = null;

			}else{
				throw new QException(ExceptionType.IncompleteParameters_err);
			}
		} catch (Exception e) {
			throw new QException(e, 
					ExceptionType.OperationFailed_err, Encryptor.class.getName() + "-encrypt");
		}
		return valueEncrypted;
	}
	
	/**
	 * Decrypt a String value with AES Standard
	 * @param key secure Key to encrypt the value
	 * @param initVector IvParameter Specification
	 * @param valueToDecrypt Value to Encrypt
	 * @return Value Encrypted
	 * @throws QException Launch exception when something fails
	 */
	public static String decrypt(String key, String initVector, String valueToDecrypt) throws QException{
		
		String valueDecrypt = null;
		
		try {
			
			if(!TypesValidator.isNullOrEmpty(valueToDecrypt)){
				
				IvParameterSpec ivParam = new IvParameterSpec(initVector.getBytes(cryptoCharSet));
				SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(cryptoCharSet), cryptoAlgoritm);
				
				Cipher cipher = Cipher.getInstance(cryptoTransformation);
				cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParam);
				
				byte[] bytesDecrypted = cipher.doFinal(Base64.decodeBase64(valueToDecrypt));
				
				valueDecrypt = new String(bytesDecrypted);

				ivParam = null;
				secretKey = null;
				cipher = null;
				bytesDecrypted = null;
			}else{
				throw new QException(ExceptionType.IncompleteParameters_err);
			}
		} catch (Exception e) {
			throw new QException(e, 
					ExceptionType.OperationFailed_err, Encryptor.class.getName() + "-decrypt");
		}
		return valueDecrypt;
	}
	
	
}
