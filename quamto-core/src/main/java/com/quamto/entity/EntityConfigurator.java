package com.quamto.entity;

/**
 * Created by Jhon-Aleck on 11/08/17.
 */
public class EntityConfigurator {

    private static Boolean entityUseAuditFields = false;

    /**
     * Gets if the DAO objects use the audit fields
     * @return true or false
     */
    public static Boolean getEntityUseAuditFields() {
        return entityUseAuditFields;
    }

    /**
     * Sets if the DAO objects use the audit fields
     * @param entityUseAuditFields
     */
    public static void setEntityUseAuditFields(Boolean entityUseAuditFields) {
        EntityConfigurator.entityUseAuditFields = entityUseAuditFields;
    }
}
