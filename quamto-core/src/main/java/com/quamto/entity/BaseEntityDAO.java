package com.quamto.entity;

import com.quamto.core.Credential;
import com.quamto.core.QException;
import com.quamto.core.QException.ExceptionType;
import com.quamto.core.Result;
import com.quamto.db.DbConnection;
import com.quamto.db.DbOperator;
import com.quamto.db.SQLParameter;
import com.quamto.db.SQLParameter.SQLDataType;
import com.quamto.util.TypesValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Base class definition used with the functions defined in the 
 * implementation of update functions of the institutions database
 * @author jholguin
 * @since 16/12/2015
 *
 */
public abstract class BaseEntityDAO implements AutoCloseable{

	/**
	 * Defines the edition mode
	 * 
	 * @author jholguin
	 */
	public enum EditionMode{
		Nothing_em,
		Add_em,
		Update_em,
		Delete_em;
	}
	
	protected Logger logger = null;

	protected String subClassName = "";
	protected Class<?> subClass = null;
	protected DbConnection dbConnection;
	protected DbOperator dbOperator;
	
	protected String entityTableName;
	protected String entityIdFieldName;
	
	protected EditionMode currentEditionMode = EditionMode.Nothing_em;
	
	protected ArrayList<SQLParameter> queryParametersList;

	protected Credential credential = null;
	protected static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	//It allows you to store the tables and fields where the record is related
	private List<ForeignTableInformation> entityForeignTables;
	
	//Unique record identifier, common across all implementations of DAO
	protected long id;
	
	public BaseEntityDAO(DbConnection dbConnection, String entityTableName, String entityIdFieldName, 
			Class<?> subClass){
		this.dbConnection = dbConnection;
		this.dbOperator = new DbOperator(dbConnection);
		this.entityTableName = entityTableName;
		this.entityIdFieldName = entityIdFieldName;
		this.subClass = subClass;
		this.subClassName = subClass.getName();
		this.logger = LogManager.getLogger(subClass);
	}
	
	/**
	 * Run pre-registration action of adding operations in the database
	 * @param entity Entity to validate
	 * @return Result of the validation operation
	 */
	public Result executePreviousValidationsToAdd(BaseEntity entity){
		return new Result();
	}
	
	/**
	 * Run pre-registration action to upgrade operations in the database
	 * @param entity Entity to validate
	 * @return Result of the validation operation
	 */
	public Result executePreviousValidationsToUpdate(BaseEntity entity){
		return new Result();
	}
	
	public abstract List<?> getAll() throws QException;
	
	/**
	 * Responsible for loading the values of the attributes of the class to use as parameters in the 
	 * CRUD SQL queries, they are loaded into the attribute listaParametrosSQL
	 */
	protected abstract void loadQueryParameters(BaseEntity entity) throws QException;
	
	/**
	 * Charging from a result set attributes of the entity assigning the class
	 * 
	 * @param rs Result set that contains the data
	 * 
	 */
	protected abstract BaseEntity loadEntityAttributesFromRs(ResultSet rs) throws QException;
	
	/**
	 * Gets an entity charged object subclass
	 * 
	 * @param id Record identifier in the repository database
	 * @return Object type subclass loaded
	 * @throws QException Throws an exception if there are any problems in loading
	 */
	public BaseEntity get(Long id) throws QException{
		return getEntityLoaded(id);
	}
	
	/**
	 * Load an object of the given model entity
	 * @param id Entity identifier to be loaded
	 * @return Defined entity model (the object must be instantiated)
	 * @throws QException Launch exception when something fails
	 */
	protected BaseEntity getEntityLoaded(Long id) throws QException{
		
		BaseEntity entity = null;
		
		try {
		
			String query = "SELECT * " 
		            + "  FROM " + entityTableName
					+ " WHERE " + entityIdFieldName + "=" + id;
			entity = getEntityLoaded(query);
			
		} catch (Exception e) {
			throw new QException(e, ExceptionType.NotIdentified_err, subClassName + "-" + "getEntityLoaded");
		}
		return entity;
	}
	
	/**
	 * Load an object of the given model entity
	 * @param queryFilter filter to get a entity loaded (Must to be unique),
	 * not include the WHERE sentence for the first filter
	 * @return Defined entity model (the object must be instantiated)
	 * @throws QException Launch exception when something fails
	 */
	protected BaseEntity getEntityLoadedFiltered(String queryFilter) throws QException{
		
		BaseEntity entity = null;
		
		try {
			
			if(!TypesValidator.isNullOrEmpty(queryFilter)){
				String query = "SELECT * " 
					         + "  FROM " + entityTableName 
					         + " WHERE " + queryFilter;
				
				entity = getEntityLoaded(query);
			}
		} catch (Exception e) {
			throw new QException(e, ExceptionType.NotIdentified_err, subClassName + "-" + "getEntityLoadedFiltered");
		}
		return entity;
	}
	
	/**
	 * Load an object of the given model entity
	 * @param query Entity identifier to be loaded
	 * @return Defined entity model (the object must be instantiated)
	 * @throws QException Launch exception when something fails
	 */
	protected BaseEntity getEntityLoaded(String query) throws QException{
		
		BaseEntity entity = null;
		
		try {
		
			ResultSet rs = null;
			
			try {
				
				rs = dbOperator.getResultSet(query);
				while(rs.next()){
					
					entity = loadEntityAttributesFromRs(rs);
					break;
				}
				closeResultSet(rs);
			} catch (Exception e) {
				throw new QException(ExceptionType.InvalidIdentifier_err,
						subClassName + "getEntityLoaded");
			}finally{
				
				if(rs != null){
					rs.close();
				}
			}
		} catch (Exception e) {
			throw new QException(e, ExceptionType.NotIdentified_err, subClassName + "-" + "getEntityLoaded");
		}
		return entity;
	}
	
	/**
	 * Performs validation process using the registration database 
	 * in order to preserve the integrity of the database
	 * @param id Record identifier to be validated
	 * @return True if the record is currently linked to other tables, false otherwise
	 * 
	 */
	public Result validateDbIntegrityRecord(Long id){
		
		Result result = new Result();
		try {
			this.setupForeignTables();
			
			if(entityForeignTables != null){
				String tables = "";
				String separator = "";
				
				for (int i = 0; i < entityForeignTables.size(); i++) {
					
					ForeignTableInformation ftInfo = entityForeignTables.get(i);
					if(dbOperator.existValueInTable(ftInfo.getTableName(), ftInfo.getFieldName(), id) == true){
						
						tables += separator + ftInfo.getAliasName();
						separator = ",";
					}
				}
				if(!tables.isEmpty()){
					
					result.setException(new QException(ExceptionType.DataIntegrity_err, 
							subClassName + "-validateDbIntegrityRecord", ""));
				}
			}
		} catch (Exception e) {
			result.setException(new QException(e));
		}
		return result;
	}

	/**
	 * Sets the entity control attributes for record auditory
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public BaseEntity setEntityControlAttributes(BaseEntity entity, EditionMode mode) throws Exception{
		try{
			//Validates if the DAO objects manage the audit fields
			if(EntityConfigurator.getEntityUseAuditFields()){
				Credential credential = this.credential;
				if(credential == null){
					credential = new Credential();
					credential.setUser("Anonymous");
				}

				switch (mode){
					case Add_em:
						entity.setCreationUser(credential.getUser());
						entity.setCreationDate(new Timestamp(System.currentTimeMillis()));
						break;
					case Update_em:
						entity.setUpdateUser(credential.getUser());
						entity.setUpdateDate(new Timestamp(System.currentTimeMillis()));
				}

				addQueryParameter("creator", entity.getCreationUser(), SQLDataType.String_sdt);
				addQueryParameter("created_date", entity.getCreationDate(), SQLDataType.DateTime_sdt);
				addQueryParameter("modifier", entity.getUpdateUser(), SQLDataType.String_sdt);
				addQueryParameter("modification_date", entity.getUpdateDate(), SQLDataType.DateTime_sdt);
			}
		}catch (Exception e){
			throw e;
		}
		return entity;
	}

	/**
	 * Adds a record of the entity to database
	 * 
	 * @param entity Specific DAO object which you want to add to the database
	 * @return result of the operation added to the registration database
	 */
	public Result add(BaseEntity entity){
		
		Result result = new Result();
		try {
			
			result = executePreviousValidationsToAdd(entity);
			
			if(result.isSuccessful()){
				queryParametersList = new ArrayList<SQLParameter>();
				currentEditionMode = EditionMode.Add_em;
				
				loadQueryParameters(entity);
				entity = setEntityControlAttributes(entity, currentEditionMode);
				result = dbOperator.executeInsertQuery(entityTableName, queryParametersList);
				if(result.getInfoSQL() != null){
					entity.setID(result.getInfoSQL().getLastId());
				}
			}
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.NotIdentified_err, subClassName + "-" + "add"));
		}
		currentEditionMode = EditionMode.Nothing_em;
		return result;
	}
	
	/**
	 * Upgrade to register the identity given in the database
	 * 
	 * @param entity Specific DAO object which you want to update to the database
	 * @return result of the operation added to the registration database
	 */
	public Result update(BaseEntity entity){
		
		Result result = new Result();
		
		try {
			
			result = executePreviousValidationsToUpdate(entity);
			
			if(result.isSuccessful()){
				queryParametersList = new ArrayList<SQLParameter>();
				currentEditionMode = EditionMode.Update_em;
				
				loadQueryParameters(entity);
				entity = setEntityControlAttributes(entity, currentEditionMode);
				result = dbOperator.executeUpdateQuery(entityTableName, entityIdFieldName, entity.ID, queryParametersList);	
			}
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.NotIdentified_err, subClassName + "-" + "update"));
		}
		currentEditionMode = EditionMode.Nothing_em;
		return result;
		
	}
	
	/**
	 * Eliminates record the identity given database
	 * @param id Entity identifier
	 * @return result of the operation deleted to the registration database
	 */
	public Result delete(long id){
		
		Result result = new Result();
		
		try {
			
			result = validateDbIntegrityRecord(id);
			
			if(result.isSuccessful()){
				
				currentEditionMode = EditionMode.Delete_em;
				
				String sentenciaSQL = "DELETE FROM " + entityTableName 
						+ " WHERE " + entityIdFieldName + "=" + id ;
				
				result = dbOperator.executeQuery(sentenciaSQL);
			}
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.NotIdentified_err, subClassName + "-" + "delete"));
		}
		currentEditionMode = EditionMode.Nothing_em;
		return result;
	}
	
	/**
	 * Executes an SQL statement in the database
	 * 
	 * @param query SQL statement to be executed
	 * @return Returns the result of the execution of the SQL statement
	 */
	public Result executeQuery(String query){
		
		Result result = new Result();
		
		try {
			
			result = dbOperator.executeQuery(query);
		} catch (Exception e) {
			result.setException(new QException(e, ExceptionType.DatabaseOperation_err, subClassName + "-ejecutarSQL", query));
		}
		return result;
	}
	
	/**
	 * Gets the list of records in the database of the mapped table entity DAO
	 * @return Data table obtained from the database
	 * @throws QException Application throws an exception if not find it
	 */
	public ResultSet getResultSet() throws QException{
		
		ResultSet rs = null;
		
		try {
			
			String query = "SELECT * " 
					     + "  FROM " + entityTableName;
			
			rs = dbOperator.getResultSet(query);
		} catch (Exception e) {
			throw new QException(e, ExceptionType.NotIdentified_err, subClassName + "-obtenerRs");
		}
		return rs;
	}
	
	/**
	 * Gets the list of records in the database of a SQL statement defined
	 * @param query SQL you want to get a list of records
	 * @return Result table of the query
	 * @throws QException Throws an exception if the operation fails to complete
	 */
	public ResultSet getResultSet(String query) throws QException{
		
		ResultSet rs = null;
		
		try {
			
			rs = dbOperator.getResultSet(query);
		} catch (Exception e) {
			throw new QException(e, ExceptionType.NotIdentified_err, subClassName + "-obtenerRs", query);
		}
		return rs;
	}
	
	
	/**
	 * Add SQL parameters of the entity to CRUD
	 * @param fieldName Field name that is associated with the SQL parameter
	 * @param value Value assigned to the field
	 * @param type Type value assigned to the SQL parameter field
	 */
	protected void addQueryParameter(String fieldName, Object value, SQLDataType type) throws QException{
		addQueryParameter(fieldName, value, type, false);
	}
	
	/**
	 * Add SQL parameters of the entity to CRUD
	 * @param fieldName Field name that is associated with the SQL parameter
	 * @param value Value assigned to the field
	 * @param type Type value assigned to the SQL parameter field
	 * @param isMandatory If the field is mandatory
	 */
	protected void addQueryParameter(String fieldName, Object value, SQLDataType type, 
			Boolean isMandatory) throws QException{
	
		addQueryParameter(fieldName, value, type, isMandatory, "");
	}
	
	
	/**
	 * Add SQL parameters of the entity to CRUD
	 * @param fieldName Field name that is associated with the SQL parameter
	 * @param value Value assigned to the field
	 * @param type Type value assigned to the SQL parameter field
	 * @param isMandatory If the field is mandatory
	 * @param conditionFieldAI It sets whether there is a condition to assign the auto-increment value
	 */
	protected void addQueryParameter(String fieldName, Object value, SQLDataType type, 
			Boolean isMandatory, String conditionFieldAI) throws QException{
		
		try {
			
			if(queryParametersList!=null){
				
				if(isMandatory && value == null){
					
					throw new QException(ExceptionType.IncompleteParameters_err, 
							subClassName + "addQueryParameter");
					
				}else{
					
					queryParametersList.add(new SQLParameter(fieldName, value, type, isMandatory, conditionFieldAI));
				}
			}else{
				throw new QException(ExceptionType.DatabaseOperation_err, 
						subClassName + "-addQueryParameter");
			}
			
		} catch (Exception e) {
			throw new QException(e, ExceptionType.NotIdentified_err, subClassName + "-" + "-addQueryParameter");
		}
		
	}
	
	/**
	 * It is giving the SQL operator used by the base entity class DAO
	 * 
	 * @return Database operator
	 */
	public DbOperator getDbOperator(){
		return dbOperator;
	}
	
	/**
	 * Sets the sql operator
	 * @param sqlOperator SQL operator with the database connection established
	 */
	public void setDbOperator(DbOperator sqlOperator){
		this.dbOperator = sqlOperator;
	}
	
	/**
	 * Gets the connection database object
	 * @return Database connection
	 */
	public DbConnection getDbConnection(){
		return this.dbConnection;
	}
	
	/**
	 * Sets the connection database object
	 * @param dbConnection Database connection opened
	 */
	public void setDbConnection(DbConnection dbConnection){
		this.dbConnection = dbConnection;
	}

	/**
	 * Gets a list with attributes and values mapped from a query
	 * @param query Database query
	 * @return
	 * @throws QException
	 */
	public List<Map<String, Object>> mapArrayFromQuery(String query) throws QException {
		return dbOperator.getMapArrayFromQuery(query);
	}
	
	/**
	 * Establish the foreign tables relationated with the entity
	 */
	protected Result setupForeignTables(){
		return new Result();
	}
	
	/**
	 * Adds a foreign table to validate when tries delete
	 * @param tableName Table name
	 * @param fieldName Field name
	 * @throws QException Launch exception when something fails
	 */
	protected void addForeignTable(String tableName, String fieldName, String aliasName) throws QException{
		
		try {
			
			if(this.entityForeignTables == null){
				this.entityForeignTables = new ArrayList<ForeignTableInformation>();
			}
			
			entityForeignTables.add(new ForeignTableInformation(tableName, fieldName, aliasName));
		} catch (Exception e) {
			throw new QException(e, ExceptionType.OperationFailed_err, subClassName + "-addForeignTable");
		}
	}
	
	/**
	 * Close a resultSet and sets = null
	 * @param rs Resultset to close
	 */
	protected void closeResultSet(ResultSet rs){
		
		try {
			if(rs != null){
				rs.close();
			}
			rs = null;
		} catch (Exception e) {
			new QException(e, ExceptionType.OperationFailed_err, subClassName + "-closeResultSet");
		}
	}
	
	/**
	 * @return Gets the entity table name mapped with the database
	 */
	public String getEntityTableName(){
		return entityTableName;
	}
	
	/**
	 * @return Gets the id entity field name of the entity table
	 */
	public String getIdEntityFieldName(){
		return entityIdFieldName;
	}
	
	/**
	 * Validate if the DAO table exist in the database
	 * @return True if the table exists
	 */
	public boolean validateTableExist(){
		boolean exist = false;
		try {
			ResultSet rs = dbOperator.getResultSet("SELECT " + entityIdFieldName +
					                               "  FROM " + entityTableName +
					                               " WHERE 1=2");
			exist = true;
			rs.close();
		} catch (Exception e) {
			logger.warn("Table validation error " + e.getMessage());
		}
		return exist;
	}

	/**
	 * @return User credentials owner of the DAO operation
	 */
	public Credential getCredential() {
		return credential;
	}

	/**
	 * Sets the user credentials for the DAO operations
	 * @param credential User credentials
	 */
	public void setCredential(Credential credential) {
		this.credential = credential;
	}

}
