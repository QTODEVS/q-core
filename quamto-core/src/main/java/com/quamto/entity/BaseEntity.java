package com.quamto.entity;

import java.sql.Timestamp;
import java.util.Date;


/**
 * Entity base class for models 
 * 
 * @author jholguin
 * @since 16/12/2015
 *
 */
public class BaseEntity {
	

	protected Long ID;
	protected Timestamp creationDate = null;
	protected String creationUser = null;
	protected Timestamp updateDate = null;
	protected String updateUser = null;
	
	/**
	 * 
	 */
	public BaseEntity(){
		
	}
	
	
	/**
	 * @return Register identifier
	 */
	public Long getID() {
		return this.ID;
	}

	/**
	 * @param id identifier
	 */
	public void setID(Long id) {
		this.ID = id;
	}
	
	/**
	 * @return Date when the record was created
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}


	/**
	 * @param value when the record was created
	 */
	public void setCreationDate(Timestamp value) {
		this.creationDate = value;
	}


	/**
	 * @return Login of the user who created the record
	 */
	public String getCreationUser() {
		return creationUser;
	}


	/**
	 * @param value Login of the user who created the record
	 */
	public void setCreationUser(String value) {
		this.creationUser = value;
	}


	/**
	 * @return Record Updating Date
	 */
	public Timestamp getUpdateDate() {
		return updateDate;
	}


	/**
	 * @param value Record Updating Date
	 */
	public void setUpdateDate(Timestamp value) {
		this.updateDate = value;
	}


	/**
	 * @return User login you update the record
	 */
	public String getUpdateUser() {
		return updateUser;
	}


	/**
	 * @param value User login you update the record
	 */
	public void setUpdateUser(String value) {
		this.updateUser = value;
	}
}
