package com.quamto.entity;

/**
 * Contains the foreign table information
 * used to validate the data integrity
 * 
 * @author jholguin
 * @since 16/12/2015
 *
 */
public class ForeignTableInformation {
	
	private String tableName = null;
	private String fieldName = null;
	private String aliasName = null;
	
	public ForeignTableInformation(String tableName, String fieldName, String aliasName) {
		this.tableName = tableName;
		this.fieldName = fieldName;
		this.aliasName = aliasName;
	}
	
	/**
	 * @return Foreign table name 
	 */
	public String getTableName() {
		return tableName;
	}
	/**
	 * @param tableName Foreign table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	/**
	 * @return Foreign field name 
	 */
	public String getFieldName() {
		return fieldName;
	}
	/**
	 * @param fieldName oreign field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	/**
	 * @return Foreign alias name to show
	 */
	public String getAliasName() {
		return aliasName;
	}
	/**
	 * @param tableAlias alias name to show
	 */
	public void setAliasName(String tableAlias) {
		this.aliasName = tableAlias;
	}
}
